﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Skoda.PCMP.Common;
using Skoda.PCMP.Common.Helpers;
using Skoda.PCMP.Data.Models;
using Skoda.PCMP.Models;
using Skoda.PCMP.Services;
using Skoda.PCMP.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Skoda.PCMP.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class AdminDealerEnquiryController : BaseController
    {
        public readonly ILogger<AdminDealerEnquiryController> _logger;
        public SMTPSettings _smtpSettings;

        public AdminDealerEnquiryController(IWebHostEnvironment hostingEnvironment, ILogger<AdminDealerEnquiryController> logger, IConfiguration config, SkodaContext skodaContext, IOptions<SMTPSettings> smtpSettings)
            : base(hostingEnvironment, config, skodaContext)
        {
            _logger = logger;
            _smtpSettings = smtpSettings.Value;
        }

        public IActionResult Index()
        {
            UserDetails uInfo = null;

            if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
            {
                uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }
            ViewBag.UserName = uInfo.Username;
            ViewBag.Role = uInfo.Role;
            return View();
        }

      

    }
}