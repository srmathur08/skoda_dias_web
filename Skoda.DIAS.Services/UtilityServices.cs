﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace Skoda.DIAS.Services
{
    public class UtilityServices
    {
        public static void GenerateAndSaveThumbForVideo(string wwwrootPath, string postVideoUrl)
        {
            Process ffmpeg; // creating process
            string video;
            string mpg;
            video = postVideoUrl;
            mpg = postVideoUrl.Substring(0, postVideoUrl.LastIndexOf(".")) + "_Thumb.jpg"; // thumb name with path !
            ffmpeg = new Process();
            ffmpeg.StartInfo.Arguments = " -i \"" + video + "\" -vframes 1 -f image2 -vcodec mjpeg \"" + mpg + "\""; // arguments !
            ffmpeg.StartInfo.FileName = $"{wwwrootPath}/ffmpeg.exe";
            ffmpeg.StartInfo.UseShellExecute = false;
            ffmpeg.StartInfo.CreateNoWindow = true;
            ffmpeg.StartInfo.RedirectStandardOutput = false;
            ffmpeg.Start();
        }

        public static void UploadBase64String(string AssetPath, string FileName, string UploadedFile, string WwwrootPath, bool IsVideo)
        {
            if (string.IsNullOrEmpty(UploadedFile)) return;

            byte[] assetByte = Convert.FromBase64String(UploadedFile);

            // file path
            string assetUrl = $"{AssetPath}/{FileName}";

            //create a folder based on the Id
            if (!Directory.Exists(AssetPath))
                Directory.CreateDirectory(AssetPath);

            var fs = new BinaryWriter(new FileStream(assetUrl, FileMode.OpenOrCreate, FileAccess.Write));
            fs.Write(assetByte);
            fs.Close();

            if (IsVideo)
            {
                GenerateAndSaveThumbForVideo(WwwrootPath, assetUrl);
            }
        }

        public static bool UploadBase64String(string ImagePath, string FileName, string UploadedFile, string FTPUrl, string FTPUsername = "", string FTPPassword = "")
        {
            bool result = false;

            if (!string.IsNullOrEmpty(ImagePath))
            {
                FtpWebRequest requestDir = (FtpWebRequest)WebRequest.Create(string.Format("{0}/{1}", FTPUrl, ImagePath));
                requestDir.Method = WebRequestMethods.Ftp.MakeDirectory;

                if (!string.IsNullOrEmpty(FTPUsername) && !string.IsNullOrEmpty(FTPPassword))
                {
                    requestDir.Credentials = new NetworkCredential(FTPUsername, FTPPassword);
                }

                _ = (FtpWebResponse)requestDir.GetResponse();
            }

            string FilePath = string.IsNullOrEmpty(ImagePath) ? string.Format("{0}/{1}", FTPUrl, FileName) : string.Format("{0}/{1}/{2}", FTPUrl, ImagePath, FileName);
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FilePath);
            request.Method = WebRequestMethods.Ftp.UploadFile;

            if (!string.IsNullOrEmpty(FTPUsername) && !string.IsNullOrEmpty(FTPPassword))
            {
                request.Credentials = new NetworkCredential(FTPUsername, FTPPassword);
            }

            if (string.IsNullOrEmpty(UploadedFile)) return result;

            byte[] imageByte = Convert.FromBase64String(UploadedFile);
            request.ContentLength = imageByte.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(imageByte, 0, imageByte.Length);
            }

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                result = true;
            }

            return result;
        }
    }
}
