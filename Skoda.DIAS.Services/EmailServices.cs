﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Skoda.DIAS.Services
{
    public class EmailServices
    {
        public static void SendSMS(string Mobile, string Message)
        {
            RestSharp.RestClient client = new RestSharp.RestClient("https://api.msg91.com/api/v2/sendsms");
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authkey", "320579AJn1rTFYOmTo5e58e488P1");
            request.AddParameter("application/json", "{ \"sender\": \"SKODA\", \"route\": \"4\", \"country\": \"0\", \"sms\": [ { \"message\": \"" + Message + "\", \"to\": [ \"" + Mobile + "\" ] } ] }", RestSharp.ParameterType.RequestBody);
            RestSharp.IRestResponse response = client.Execute(request);
        }
        public static void SendMail(string Host, int Port, string Username, string Password, bool EnableSsl,
            string FromAddress, string FromName, string ToAddress,
            string Body, string Subject, bool IsBodyHtml, SendCompletedEventHandler callback, string[] BccAddress = null)
        {
            try
            {
                SmtpClient client = new SmtpClient(Host);
                client.EnableSsl = EnableSsl;
                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                if (Port > 0)
                    client.Port = Port;

                if (!string.IsNullOrEmpty(Username))
                    client.Credentials = new NetworkCredential(Username, Password);

                MailAddress from = new MailAddress(FromAddress, FromName, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(ToAddress);
                MailMessage message = new MailMessage();
                if (BccAddress != null)
                {
                    foreach (var a in BccAddress)
                    {
                        MailAddress bcc = new MailAddress(a);
                        message.Bcc.Add(bcc);
                    }
                }
                message.From = from;
                message.To.Add(to);

                message.Body = Body;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = Subject;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = IsBodyHtml;

                if (callback != null)
                    client.SendCompleted += callback;

                client.Send(message);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
