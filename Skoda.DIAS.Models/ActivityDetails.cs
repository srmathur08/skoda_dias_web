﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    //  [Serializable]
    public class Activity
    {
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public string Delear { get; set; }
        public string EnquiryId { get; set; }
    }
    public class ActivityDetails
    {

        public int Delear_Id { get; set; }
        public int Id { get; set; }
        public string StageName { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string Created_date { get; set; }
        public DateTime Modifydate { get; set; } 
        public DateTime DocumentSubmitted { get; set; }
        public string About_businessExp { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public string Bussiness_Type { get; set; }
        public string Register_bussinessName { get; set; }
        public string isSalaried { get; set; }
        public string source { get; set; }
        public string how_know { get; set; }
        public string Salaried { get; set; }
        public string Background { get; set; }
        public string Brand_name { get; set; }
        public string OtherLocation { get; set; } 
        public List<Activity> activities { get; set; } 
    }

    public class ActivityViewDetails
    {

        public int Delear_Id { get; set; }
        public int Id { get; set; }
        public string StageName { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string Created_date { get; set; }
        public DateTime Modifydate { get; set; }
        public DateTime DocumentSubmitted { get; set; }
        public string About_businessExp { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public string Bussiness_Type { get; set; }
        public string Register_bussinessName { get; set; }

        public string source { get; set; }
        public string how_know { get; set; }
        public string Salaried { get; set; }
        public string Background { get; set; }
        public string Brand_name { get; set; }
        public string OtherLocation { get; set; }
        public List<Activity> activities { get; set; }
    }
    public class ExportDetails
    {
         
        public string StageName { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string Created_date { get; set; }
        public DateTime Modifydate { get; set; }
        public DateTime DocumentSubmitted { get; set; }
        public string About_businessExp { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public string Bussiness_Type { get; set; }
        public string Register_bussinessName { get; set; }

        public string source { get; set; }
        public string how_know { get; set; }
        public string Salaried { get; set; }
        public string Background { get; set; }
        public string Brand_name { get; set; }
        public string OtherLocation { get; set; }
        public List<Activity> activities { get; set; } 
        public string Enquiry_id { get; set; } 
        public string Mobile_No { get; set; } 
        public string Open_Since { get; set; } 
        public string Owner { get; set; }     
        public int delear_ID { get; set; }
        public string Created_Date { get; set; } 
    }
}