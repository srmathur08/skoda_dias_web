﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    [Serializable]
    public class ZoneDetails
    {  
        public int Id { get; set; }
        [Required(ErrorMessage = "ZoneName is requried")]
        public string Name { get; set; }
        public string ErrorMessage { get; set; } 
    } 
}