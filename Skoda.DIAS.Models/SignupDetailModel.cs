﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    public class SignupDetailModel
    {
        [Required(ErrorMessage = "Type Of Bussiness is required")]
        public string Bussiness_Type { get; set; } 

        public int BackgroundId { get; set; }
        public int DlelearId { get; set; }
        [Required(ErrorMessage = "Name of the Brand is required")]
        public string Brand_name { get; set; }
        [Required(ErrorMessage = "Register Name of Your Bussiness is required")]
        public string Register_BussinessName { get; set; }
        [Required(ErrorMessage = "About is required")]
        public string About_BussinessExp { get; set; }

        [Required(ErrorMessage = "Source is required")]
        public string Source { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public int CityId { get; set; }
        public bool issalaried { get; set; }
        public string Pincode { get; set; }

        public string How_Know { get; set; }
        public string salaried { get; set; }
        public string OtherLocation { get; set; }
        public string ErrorMessage { get; set; }
        public List<Itemlist> Background { get; set; }
    }
    public class Itemlist
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }
}
