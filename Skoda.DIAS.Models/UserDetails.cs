﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    [Serializable]
    public class UserDetails
    {
        private class RequireWhenAddAttribute : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var obj = (UserDetails)validationContext.ObjectInstance;
                if (obj.UserId > 0)
                    return ValidationResult.Success;

                var valueStr = value as string;
                return string.IsNullOrWhiteSpace(valueStr)
                    ? new ValidationResult(ErrorMessage)
                    : ValidationResult.Success;
            }
        }

        [Required(ErrorMessage = "Fullname is requried")]
        public string Fullname { get; set; }
        [RequireWhenAdd(ErrorMessage = "Username is requried")]
        public string Username { get; set; }
        [RequireWhenAdd(ErrorMessage = "Password is requried")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Mobile is requried")]
        public string Mobile { get; set; }
        [Required(ErrorMessage = "CityId is requried")]
        public int CityId { get; set; }
        public int UserId { get; set; }
        public string Role { get; set; }
        public int ZoneId { get; set; }
        public int RoleId { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string CityList { get; set; }
        public string ErrorMessage { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }


    }

    [Serializable]
    public class UserProfile
    {
        public string Username { get; set; }
        public int UserId { get; set; }

        [Required(ErrorMessage = "Please enter fullname")]
        public string FullName { get; set; }
        public int RoleId { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}