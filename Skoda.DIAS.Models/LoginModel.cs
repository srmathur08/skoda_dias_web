﻿using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    public class LoginModel
    { 
        [Required(ErrorMessage = "UserName is requried")] 
        public string Username { get; set; }
        [Required]
        public string Password { get; set; } 
        public string Mobile { get; set; }
        public string ErrorMessage { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
    public class ForgotPassModel
    {
        [Required(ErrorMessage = "Email is requried")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", ErrorMessage = "Invalid email format")]
        public string Username { get; set; } 
        public string ErrorMessage { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
    public class VerificationModel
    {
        public int Id { get; set; }
        public int DelearId { get; set; }
        public string Code { get; set; }
        public int SendCount { get; set; }
    }
    public class DelearLoginModel
    {
        [Required]
        public string MobileNo { get; set; }
        [Required]
        public string Otp { get; set; }
        public string ErrorMessage { get; set; } 
        public string ReturnUrl { get; set; }
    }
}
