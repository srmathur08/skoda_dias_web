﻿using Skoda.DIAS.Data.Models;
using System.Collections.Generic;

namespace Skoda.DIAS.Models
{
    public class DashboardModel
    {
        public DashboardModel()
        { 
        } 
    }
    
    public class DashboardStages
    {
        public long StageId { get; set; }
        public string StageTitle { get; set; }
        public int StageCount { get; set; }
    }
}
