﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    [Serializable]
    public class SignupModel
    {
        [Required(ErrorMessage = "Fullname is requried")]
        public string Fullname { get; set; }

        [Required(ErrorMessage = "Email is requried")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", ErrorMessage = "Invalid email format")]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "MobileNo is requried")]
        public string MobileNo { get; set; } 
        public string City { get; set; } 
        public IEnumerable<SelectListItem> Cities { get; set; }
        public IEnumerable<SelectListItem> OCities { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        [Required(ErrorMessage = "State is requried")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "City is requried")]
        public int CityId { get; set; }
        public int OCityId { get; set; } 
        public string Pincode { get; set; }
        public string ErrorMessage { get; set; }
        public string Bussiness_Type { get; set; }  
    }

}
