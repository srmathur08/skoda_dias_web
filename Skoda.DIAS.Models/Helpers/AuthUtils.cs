﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace Skoda.DIAS.Models.Helpers
{
    public static class AuthUtils
    { 

        public static string EncryptUserDetails(UserDetails uInfo)
        {
            var authData = JsonConvert.SerializeObject(uInfo);
            var authBytes = Encoding.UTF8.GetBytes(authData);

            var token = Convert.ToBase64String(authBytes);

            return token;
        }

        public static UserDetails DecryptUserDetails(string secret)
        {
            if (string.IsNullOrWhiteSpace(secret)) return null;
            var authBytes = Convert.FromBase64String(secret);
            var authData = Encoding.UTF8.GetString(authBytes);
            var uInfo = JsonConvert.DeserializeObject<UserDetails>(authData);

            return uInfo;
        }
    }
}