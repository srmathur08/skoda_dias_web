﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Skoda.DIAS.Data.Models;
using System.Collections.Generic;

namespace Skoda.DIAS.Models
{
    public class AdminDashboardModel
    {
        public AdminDashboardModel()
        {
        }
        public int TotalEnquiry { get; set; }
        public int TotalEnquiryOpen { get; set; }
        public int TotalEnquiryApproved { get; set; }
        public int TotalEnquiryOnhold { get; set; }
        public int TotalEnquiryRejected { get; set; }
        public int CityId { get; set; }
        public int ZoneId { get; set; }
        public int StateId { get; set; } 
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }
        public IEnumerable<SelectListItem> Zones { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public List<UserDelearEnquiry> userDelearEnquiries { get; set; }
    }

    public class AdminFileUpload
    {
        public int Id { get; set; }

        public string ErrorMessage { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string CreatedDate { get; set; }
    }
}
