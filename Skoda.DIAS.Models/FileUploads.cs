﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    [Serializable]
    public class FileUploads
    {  
        public int Id { get; set; } 
        public string Name { get; set; }
        public string ErrorMessage { get; set; }
        
        public bool IsDeleted { get; set; }
    } 
}