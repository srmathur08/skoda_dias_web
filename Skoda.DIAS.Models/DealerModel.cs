﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    [Serializable]
    public class Enquiry
    {
        public string Mobile { get; set; }
    }
    [Serializable]
    public class DealerModel
    {
        [Required(ErrorMessage = "Fullname is requried")]
        public string Fullname { get; set; }

        [Required(ErrorMessage = "Enquiry_id is requried")]
        public string Enquiry_id { get; set; }

        [Required(ErrorMessage = "MobileNo is requried")]
        public string Mobile_No { get; set; }
        public int Dealer_ID { get; set; }
        public int UserId { get; set; }
        public string Bussiness_Type { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Brand_name { get; set; }
    }

}
