﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Skoda.DIAS.Models
{
    public class EnquiryStatusUpdate
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public int Updated { get; set; }
        public string Comment { get; set; }
    }
    public class UserDelearEnquiry
    {
        public int Dealer_ID { get; set; }
        public string Enquiry_id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Mobile_No { get; set; }
        public string Source { get; set; }
        public string Open_Since { get; set; }
        public string NextStage { get; set; }
        public string Owner { get; set; }
        public string City { get; set; }
        public int ZoneId { get; set; }

        public int City_Id { get; set; }
        public string Pincode { get; set; }
        public string Bussiness_Type { get; set; }
        public string Register_bussinessName { get; set; }
        public string Brand_name { get; set; }
        public string About_businessExp { get; set; }
        public string How_Know { get; set; }
        public int Status_ID { get; set; }
        public string Created_Date { get; set; }
        public int Stage_Id { get; set; }
        // public string fileName { get; set; }
    }

}

