﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    [Serializable]
    public class CityDetails
    {  
        public int Id { get; set; }
        [Required(ErrorMessage = "Zone is requried")]
        public int ZoneId { get; set; }
        [Required(ErrorMessage = "Zone is requried")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "CityName is requried")]
        public string Name { get; set; }
        public bool IsOpen { get; set; }
        public string ZoneName { get; set; }
        public string StateName { get; set; }
        public string Pincode { get; set; }
        public string ErrorMessage { get; set; }
        public IEnumerable<SelectListItem> Zones { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
    }
    public class Alias
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    [Serializable]
    public class StateDetails
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Zone is requried")]
        public int ZoneId { get; set; }
        [Required(ErrorMessage = "StateName is requried")]
        public string Name { get; set; }
        public bool IsOpen { get; set; }
        public string ZoneName { get; set; } 
        public string ErrorMessage { get; set; }
        public IEnumerable<SelectListItem> Zones { get; set; }
    }
}