﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Skoda.DIAS.Models
{
    [Serializable]
    public class DealerList
    {  
        public string EnquiryId { get; set; } 
        public string FullName { get; set; }
        public string BusinessType { get; set; }
        public string BusinessName { get; set; }
        public string Brand { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public string CreatedDt { get; set; }
    } 
}