﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace Skoda.DIAS.Common
{
    public class SessionAdminExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ISession session = filterContext.HttpContext.Session;

            if (filterContext.Controller is Controller)
            {
                var ctrl = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ControllerName;
                var action = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ActionName;

                if (ctrl == "Admin" && action != "Forgotpassword" && action != "AdminLogin" && action != "Login" && action != "LoginOTP" && action != "Enquirystatus")
                {
                    if (session != null && !session.TryGetValue("UserDetails", out _))
                    {
                        filterContext.Result =
                               new RedirectToRouteResult(
                                   new RouteValueDictionary{{ "controller", "Admin" },
                                          { "action", "Login" }
                                });
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ISession session = filterContext.HttpContext.Session;

            if (filterContext.Controller is Controller)
            {
                var ctrl = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ControllerName;
                var action = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ActionName;

                 if (ctrl == "Dashboard" && (action != "Profile" || action != "Index"  || action != "List"))
                {
                    if (session != null && !session.TryGetValue("DelearLogin", out _))
                    {
                        filterContext.Result =
                               new RedirectToRouteResult(
                                   new RouteValueDictionary{{ "controller", "Delear" },
                                          { "action", "Login" }
                                });
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}