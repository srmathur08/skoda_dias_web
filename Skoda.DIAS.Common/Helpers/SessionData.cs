﻿using Microsoft.AspNetCore.Http;

namespace Skoda.DIAS.Common.Helpers
{
    public class SessionData
    {
        public static object GetUserDetails(HttpContext httpContext)
        {
            if (httpContext.Session.TryGetValue("UserDetails", out byte[] userDetailsBytes))
            {
                return ObjectToByte.ByteArrayToObject(userDetailsBytes);
            }

            return null;
        }

        public static int GetShowroomId(HttpContext httpContext)
        {
            if (httpContext.Session.TryGetValue("ShowroomId", out byte[] showroomIdBytes))
            {
                if (int.TryParse(System.Text.Encoding.UTF8.GetString(showroomIdBytes), out int showroomId))
                {
                    return showroomId;
                }
            }

            return 0;
        }

        public static object GetDealer(HttpContext httpContext)
        {
            if (httpContext.Session.TryGetValue("Dealer", out byte[] dealerBytes))
            {
                return ObjectToByte.ByteArrayToObject(dealerBytes);
            }

            return null;
        }

        public static object GetShowroom(HttpContext httpContext)
        {
            if (httpContext.Session.TryGetValue("Showroom", out byte[] ShowroomBytes))
            {
                return ObjectToByte.ByteArrayToObject(ShowroomBytes);
            }

            return null;
        }
    }
}
