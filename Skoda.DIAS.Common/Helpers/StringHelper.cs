﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Skoda.DIAS.Common.Helpers
{
    public class StringHelper
    {
        public static string ConvertToIndianFormat(string num)
        {
            try
            {
                var neg = false;
                var x = double.Parse(num);
                if (x < 0)
                {
                    x = x * -1;
                    neg = true;
                }

                if (double.IsNaN(x))
                    return num;

                if (x * 100 / 100 != 0)
                    x = double.Parse(string.Format("{0:0.00}", x));

                var x1 = x.ToString();
                var afterPoint = "";
                if (x1.IndexOf('.') > 0)
                    afterPoint = x1.Substring(x1.IndexOf('.'), x1.Length);

                x = Math.Floor(x);
                var x2 = x.ToString();
                var lastThree = x2.Substring(x2.Length - 3);
                var otherNumbers = x2.Substring(0, x2.Length - 3);
                if (!string.IsNullOrEmpty(otherNumbers))
                    lastThree = ',' + lastThree;
                var res = ((neg == true) ? "-" : "") + Regex.Replace(otherNumbers, "B(?= (d{2})+(?!d))", ",") + lastThree + afterPoint;
                return res;
            }
            catch (Exception)
            {
                return num;
            }
        }
    }
}
