﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Skoda.DIAS.Common.Helpers
{
    public class ExtApiResponse
    {
        public string Url { get; set; }
        public HttpStatusCode Status { get; set; }
        public string Response { get; set; }
    }

    public class WebRequestHelper
    {
        public static ExtApiResponse GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            try
            {
                using WebResponse response = request.GetResponse();
                HttpStatusCode ResponseCode = ((HttpWebResponse)response).StatusCode;
                if (ResponseCode.Equals(HttpStatusCode.Unauthorized))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Unauthorized - need new token" };
                }
                else if (ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Unauthorized - not permitted" };
                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Response from web service isn't OK" };
                }

                using Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                return new ExtApiResponse { Url = url, Status = HttpStatusCode.OK, Response = reader.ReadToEnd() };
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                if (errorResponse != null)
                {
                    using Stream responseStream = errorResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                }

                return new ExtApiResponse { Url = url, Status = HttpStatusCode.InternalServerError, Response = "" };
            }
        }

        public static ExtApiResponse GET(string url, string jsonContent = "")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            if (!string.IsNullOrEmpty(jsonContent))
            {
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] byteArray = encoding.GetBytes(jsonContent);

                request.ContentLength = byteArray.Length;
                request.ContentType = @"application/json";

                using Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            request.Method = "GET";

            try
            {
                using HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                HttpStatusCode ResponseCode = response.StatusCode;
                if (ResponseCode.Equals(HttpStatusCode.Unauthorized))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Unauthorized - need new token" };
                }
                else if (ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Unauthorized - not permitted" };
                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Response from web service isn't OK" };
                }

                using Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                return new ExtApiResponse { Url = url, Status = HttpStatusCode.OK, Response = reader.ReadToEnd() };
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                if (errorResponse != null)
                {
                    using Stream responseStream = errorResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                }

                return new ExtApiResponse { Url = url, Status = HttpStatusCode.InternalServerError, Response = "" };
            }
        }

        // POST a JSON string
        public static ExtApiResponse POST(string url, string jsonContent)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            try
            {
                using HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                HttpStatusCode ResponseCode = response.StatusCode;
                if (ResponseCode.Equals(HttpStatusCode.Unauthorized))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Unauthorized - need new token" };
                }
                else if (ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Unauthorized - not permitted" };
                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    return new ExtApiResponse { Url = url, Status = ResponseCode, Response = "Response from web service isn't OK" };
                }

                using Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                return new ExtApiResponse { Url = url, Status = HttpStatusCode.OK, Response = reader.ReadToEnd() };
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                if (errorResponse != null)
                {
                    using Stream responseStream = errorResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                }

                return new ExtApiResponse { Url = url, Status = HttpStatusCode.InternalServerError, Response = "" };
            }
        }
    }
}
