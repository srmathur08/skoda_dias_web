﻿namespace Skoda.DIAS.Common.Enums
{
    public enum Status : int
    {
        Open = 1,
        OnHold = 2,
        Rejected = 3,
        Approved = 4
    }
    public enum Stage : int
    {
        RequestDoc = 1,
        PredueDiligence = 2,
        DueDiligence = 3,
        Presentation = 4,
        FinalStage=5
    }
}
