﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Skoda.DIAS.Common
{
    public class EncryptDecryptUtils
    {
        /// <summary>
        /// Encrpyts the sourceString, returns this result as an Aes encrpyted, BASE64 encoded string
        /// </summary>
        /// <param name="passPhrase">The pass phrase.</param>
        /// <param name="plainSourceStringToEncrypt">a plain, Framework string (ASCII, null terminated)</param>
        /// <returns>
        /// returns an Aes encrypted, BASE64 encoded string
        /// </returns>
        public static string Encrypt(string passPhrase, string plainSourceStringToEncrypt)
        {
            //Set up the encryption objects
            using (AesCryptoServiceProvider acsp = GetProvider(Encoding.UTF8.GetBytes(passPhrase)))
            {
                byte[] sourceBytes = Encoding.UTF8.GetBytes(plainSourceStringToEncrypt);
                var key = Convert.FromBase64String(passPhrase);
                var iv = Convert.FromBase64String(passPhrase);
                ICryptoTransform ictE = acsp.CreateEncryptor();

                //Set up stream to contain the encryption
                MemoryStream msS = new MemoryStream();

                //Perform the encrpytion, storing output into the stream
                CryptoStream csS = new CryptoStream(msS, ictE, CryptoStreamMode.Write);
                csS.Write(sourceBytes, 0, sourceBytes.Length);
                csS.FlushFinalBlock();

                //sourceBytes are now encrypted as an array of secure bytes
                byte[] encryptedBytes = msS.ToArray(); //.ToArray() is important, don't mess with the buffer

                //return the encrypted bytes as a BASE64 encoded string
                return Convert.ToBase64String(encryptedBytes);
            }
        }

        /// <summary>
        /// Decrypts a BASE64 encoded string of encrypted data, returns a plain string
        /// </summary>
        /// <param name="passPhrase">The passphrase.</param>
        /// <param name="base64StringToDecrypt">an Aes encrypted AND base64 encoded string</param>
        /// <returns>returns a plain string</returns>
        public static string Decrypt(string passPhrase, string base64StringToDecrypt)
        {
            string plaintext = "";
            //Set up the encryption objects
            using (AesCryptoServiceProvider acsp = GetProvider(Encoding.UTF8.GetBytes(passPhrase)))
            {
                byte[] RawBytes = Convert.FromBase64String(base64StringToDecrypt);
                if (RawBytes == null)
                {
                    return plaintext;
                }


                var key = Convert.FromBase64String(passPhrase);
                var iv = Convert.FromBase64String(passPhrase);
                ICryptoTransform ictD = acsp.CreateDecryptor();

                //RawBytes now contains original byte array, still in Encrypted state

                //Decrypt into stream
                MemoryStream msD = new MemoryStream(RawBytes);
                CryptoStream csD = new CryptoStream(msD, ictD, CryptoStreamMode.Read);

                using (StreamReader str = new StreamReader(csD))
                {
                    plaintext = str.ReadToEnd();
                }

                return plaintext;
            }
        }

        private static AesCryptoServiceProvider GetProvider(byte[] key)
        {
            AesCryptoServiceProvider result = new AesCryptoServiceProvider();
            result.BlockSize = 128;
            result.KeySize = 256;
            result.Padding = PaddingMode.PKCS7;
            result.IV = Encoding.UTF8.GetBytes("@1B2c3D4e5F6g7H8");
            result.Key = key;

            return result;
        }

        private static byte[] GetKey(byte[] suggestedKey, SymmetricAlgorithm p)
        {
            byte[] kRaw = suggestedKey;
            List<byte> kList = new List<byte>();

            for (int i = 0; i < p.LegalKeySizes[0].MinSize; i += 8)
            {
                kList.Add(kRaw[(i / 8) % kRaw.Length]);
            }
            byte[] k = kList.ToArray();
            return k;
        }
    }
}
