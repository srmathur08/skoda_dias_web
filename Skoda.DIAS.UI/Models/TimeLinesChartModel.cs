﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Skoda.DIAS.UI.Models
{
    public class TimeLinesChartModel
    {
        public string category { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string color { get; set; }
        public string crColor { get; set; }
        public string task { get; set; }
        public List<CRTimeLineModel> crTimeLineModel { get; set; }

    }

    public class CRTimeLineModel
    {
        public string timelineName { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string category { get; set; }
        public string color { get; set; }

    }
}
