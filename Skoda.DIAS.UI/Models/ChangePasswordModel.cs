﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Skoda.DIAS.UI.Models
{
    public class ChangePasswordModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Required!")]
        public string OldPassword { get; set; }


        [Required(ErrorMessage = "Required!")]
        public string NewPassword { get; set; }


        [Required(ErrorMessage = "Required!")]
        public string ConfirmPassword { get; set; }
    }
}
