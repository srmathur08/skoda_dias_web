﻿namespace Skoda.DIAS.UI.Models
{
    public class SMTPSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public bool EnableSsl { get; set; }
        public string BccAddress { get; set; }
        public string OpenCityAdd { get; set; }  
    }
}
