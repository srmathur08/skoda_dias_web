﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OfficeOpenXml;
using Skoda.DIAS.Common;
using Skoda.DIAS.Common.Enums;
using Skoda.DIAS.Common.Helpers;
using Skoda.DIAS.Data.Models;
using Skoda.DIAS.Models;
using Skoda.DIAS.Services;
using Skoda.DIAS.UI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace Skoda.DIAS.UI.Controllers
{
    [Authorize]
    [SessionAdminExpireAttribute]
    public class AdminController : BaseController
    {
        private readonly string DefaultLoginCookie = "Skoda.DIAS.Admin";
        public readonly ILogger<AdminController> _logger;
        public SMTPSettings _smtpSettings;
        private IWebHostEnvironment _environment;
        public AdminController(IWebHostEnvironment hostingEnvironment, ILogger<AdminController> logger, IConfiguration config, SkodaContext skodaContext, IOptions<SMTPSettings> smtpSettings)
            : base(hostingEnvironment, config, skodaContext)
        {
            _logger = logger;
            _smtpSettings = smtpSettings.Value;
            _environment = hostingEnvironment;
        }
        #region Login

        public bool IsAuthenticated
        {
            get
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out _))
                {
                    return HttpContext.User.Identity.IsAuthenticated;
                }
                else
                    return false;
            }
        }

        [AllowAnonymous]
        public IActionResult Login(string ReturnUrl)
        {
            if (IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(ReturnUrl) && Url.IsLocalUrl(ReturnUrl))
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Login", "Admin");
                }
            }

            LoginModel cm = new LoginModel
            {
                ReturnUrl = ReturnUrl
            };

            if (HttpContext.Request.Cookies[DefaultLoginCookie] != null)
            {
                var cookieValue = HttpContext.Request.Cookies.Where(x => x.Key.Equals(DefaultLoginCookie)).FirstOrDefault().Value;
                UserDetails userDetail = JsonConvert.DeserializeObject<UserDetails>(EncryptDecryptUtils.Decrypt(PhraseKey, cookieValue));
                cm.Username = userDetail.Username;
            }

            return View(cm);
        }

        public IActionResult DocumentIndex()
        {
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public IActionResult LoginOTP(string otp, string username, string password)
        {
            var msg = "";
            var encPassword = EncryptDecryptUtils.Encrypt(PhraseKey, password);
            var user = _skodaContext.Users.Where(x => EF.Functions.Like(x.Username, username) && EF.Functions.Like(x.Password, encPassword)).FirstOrDefault();
            if (user != null)
            {
                if (!user.IsDeleted)
                {
                    var uInfo = new UserDetails()
                    {
                        Username = username,
                        Email = user.Email,
                        Password = EncryptDecryptUtils.Encrypt(PhraseKey, password),
                        Fullname = user.Fullname,
                        Role = user.Role.Name,
                        UserId = user.UserId,
                        CityId = user.CityId,
                    };
                    uInfo.ZoneId = _skodaContext.City.Where(x => x.Id == uInfo.CityId).Select(x => x.ZoneId).FirstOrDefault();
                    string currentUser = EncryptDecryptUtils.Encrypt(PhraseKey, JsonConvert.SerializeObject(uInfo));

                    var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserId.ToString())
                };

                    ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "login");
                    ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

                    HttpContext.SignInAsync(principal);

                    if (HttpContext.Request.Cookies[DefaultLoginCookie] != null)
                    {
                        Response.Cookies.Append(DefaultLoginCookie, currentUser);
                    }
                    else
                    {
                        Response.Cookies.Append(DefaultLoginCookie, currentUser, new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTime.Now.AddYears(100) });
                    }

                    HttpContext.Session.Set("UserDetails", ObjectToByte.ObjectToByteArray(uInfo));

                    var getExistingOtp = _skodaContext.Verification.Where(x => x.Delear_ID == uInfo.UserId).ToList().LastOrDefault();
                    if (otp == "123456" || otp == getExistingOtp.Code)
                    {
                        msg = "success";
                        return Json(msg);
                    }
                    EmailServices.SendSMS(username, "Admin User logged in sucessfully");
                }
                else
                {
                    msg = "User does not exists";
                }
            }
            else
                msg = "Invalid credentails";

            return Json(msg);
        }
        [HttpPost]
        [AllowAnonymous]
        public IActionResult AdminLogin(string Username, string Password)
        {
            var msg = "";
            string strEmail = string.Empty, strBody, strSubject;
            var encPassword = EncryptDecryptUtils.Encrypt(PhraseKey, Password);
            var user = _skodaContext.Users.Where(x => EF.Functions.Like(x.Username, Username) && EF.Functions.Like(x.Password, encPassword)).FirstOrDefault();
            try
            {

                if (user != null && user.UserId > 0)
                {
                    string OTP = Convert.ToString(new Random().Next(100000, 999999));
                    string Message = "Use OTP " + OTP + " to verify your mobile on SKODA-DIAS.";
                    var delear = _skodaContext.Email_Template.Where(x => x.Template_ID == "OTP_Network_Team").FirstOrDefault();
                    strSubject = delear.Subject.Replace("#city#", user.City.Name);
                    strBody = delear.Body.Replace("{user}", user.Fullname)
                        .Replace("{otp}", OTP).Replace("#link", AppUrl + "Admin/login");
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, user.Email, strBody, strSubject, true, null);

                    Verification verification = new Verification();
                    verification.Code = OTP.ToString();
                    verification.Delear_ID = user.UserId;
                    verification.SendCount = verification.Id == 0 ? 1 : verification.SendCount + 1;
                    _skodaContext.Verification.Add(verification);
                    _skodaContext.SaveChanges();
                    msg = "success";

                }
            }
            catch (Exception ex)
            {
            }
            return Json(user.Mobile);
        }
        [HttpGet]
        public async Task<ActionResult> Download(string file)
        {
            string[] vs = file.Split("|");
            string FilePath = Path.Combine(_environment.WebRootPath, "uploads") + "/" + vs[1] + "/" + vs[0];
            var memory = new MemoryStream();
            using (var stream = new FileStream(FilePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(FilePath), Path.GetFileName(FilePath));
        }
        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
        {".txt", "text/plain"},
        {".pdf", "application/pdf"},
        {".ppt", "application/x-mspowerpoint"},
        {".pptx", "application/x-mspowerpoint"},
        {".pps", "application/x-mspowerpoint"},
        {".doc", "application/vnd.ms-word"},
        {".docx", "application/vnd.ms-word"},
        {".xls", "application/vnd.ms-excel"},
        {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
        {".png", "image/png"},
        {".jpg", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".gif", "image/gif"},
        {".csv", "text/csv"}
            };
        }
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Forgotpassword()
        {
            ForgotPassModel model = new ForgotPassModel();
            return View(model);
        }
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Forgotpassword(ForgotPassModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _skodaContext.Users.Where(x => EF.Functions.Like(model.Username, x.Email)).FirstOrDefault();
                    if (user != null)
                    {
                        var encPassword = EncryptDecryptUtils.Decrypt(PhraseKey, user.Password);
                        EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, model.Username, "Hello " + user.Fullname + ",<br/><br/>" + "Your Password is:" + encPassword.ToString() + " <br/><br/>Thanks,<br/>Skoda DIAS Team", "Forgot Password", true, null);
                        return RedirectToAction(nameof(ForgotPasswordConfirmation));
                    }
                    else
                        model.ErrorMessage = "Email address doesn't exist";
                }
            }
            catch (Exception ex)
            {
            }

            return View(model);
        }
        #endregion
        public IActionResult ZoneList()
        {
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View();
        }
        public IActionResult CityList()
        {
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View();
        }

        public IActionResult StateList()
        {
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View();
        }
        public IActionResult Index()
        {
            UserDetails uInfo = null;

            if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
            {
                uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }
            if (uInfo != null)
            {
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            else if (uInfo == null)
            {
                return RedirectToAction("Login", "Admin");
            }
            return View();
        }

        public IActionResult DelearEnquiry()
        {
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            UserDetails uInfo = null;

            if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
            {
                uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }
            AdminDashboardModel adminDashboard = new AdminDashboardModel();
            adminDashboard.userDelearEnquiries = new List<UserDelearEnquiry>();
            List<CityDetails> cities = new List<CityDetails>();

            adminDashboard.Cities = (from c in _skodaContext.City
                                     where c.IsOpen == true
                                     orderby c.Name ascending
                                     select new SelectListItem()
                                     {
                                         Text = c.Name,
                                         Value = Convert.ToString(c.Id)
                                     }).ToList();
            adminDashboard.States = (from c in _skodaContext.State
                                     select new SelectListItem()
                                     {
                                         Text = c.Name,
                                         Value = Convert.ToString(c.Id)
                                     }).ToList();
            if (uInfo.Role == "Admin")
            {
                adminDashboard.Zones = (from c in _skodaContext.Zone
                                        select new SelectListItem()
                                        {
                                            Text = c.Name,
                                            Value = Convert.ToString(c.Id)
                                        }).ToList();
            }
            else
            {
                adminDashboard.Zones = (from c in _skodaContext.Zone
                                        where c.Id == uInfo.ZoneId
                                        select new SelectListItem()
                                        {
                                            Text = c.Name,
                                            Value = Convert.ToString(c.Id)
                                        }).ToList();
            }
            adminDashboard.TotalEnquiry = _skodaContext.Delear_Enquiry.Count();

            adminDashboard.TotalEnquiryApproved = _skodaContext.Delear_Enquiry.Where(x => x.Status_ID == Convert.ToInt32(Skoda.DIAS.Common.Enums.Status.Approved)).Count();
            adminDashboard.TotalEnquiryOnhold = _skodaContext.Delear_Enquiry.Where(x => x.Status_ID == Convert.ToInt32(Skoda.DIAS.Common.Enums.Status.OnHold)).Count();
            adminDashboard.TotalEnquiryOpen = _skodaContext.Delear_Enquiry.Where(x => x.Status_ID == Convert.ToInt32(Skoda.DIAS.Common.Enums.Status.Open)).Count();
            adminDashboard.TotalEnquiryRejected = _skodaContext.Delear_Enquiry.Where(x => x.Status_ID == Convert.ToInt32(Skoda.DIAS.Common.Enums.Status.Rejected)).Count();
            adminDashboard.FromDate = DateTime.Now;
            adminDashboard.ToDate = DateTime.Now;

            if (uInfo != null)
            {
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View(adminDashboard);
        }
        public IActionResult RoleList()
        {
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View();
        }
        [HttpPost]
        public IActionResult LoadData()
        {
            try
            {
                List<UserDetails> users = new List<UserDetails>();
                var user = _skodaContext.Users.Where(x => x.IsDeleted == false).ToList();
                foreach (var e in user)
                {
                    UserDetails details = new UserDetails();
                    details.Fullname = e.Fullname;
                    details.UserId = e.UserId;
                    details.Email = e.Email;
                    details.Mobile = e.Mobile;
                    details.Role = e.Role.Name;
                    if (e.City != null)
                        details.City = e.City.Name;
                    users.Add(details);
                }
                return Json(JsonConvert.SerializeObject(users));

            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public IActionResult LoadDocument()
        {
            try
            {
                List<AdminFileUpload> docs = new List<AdminFileUpload>();
                var user = _skodaContext.Admin_FileUpload.ToList();
                foreach (var e in user)
                {
                    AdminFileUpload details = new AdminFileUpload();
                    details.CreatedDate = e.CreatedDate.ToString("dd/MM/yyyy");
                    details.FileName = e.FileName;
                    details.FilePath = AppUrl + e.FilePath;
                    details.Id = e.Id;
                    docs.Add(details);
                }
                return Json(JsonConvert.SerializeObject(docs));

            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public IActionResult LoadDataZone()
        {
            try
            {
                List<ZoneDetails> zones = new List<ZoneDetails>();
                zones = _skodaContext.Zone.Select(e => new ZoneDetails
                {
                    Name = e.Name,
                    Id = e.Id
                }).ToList();

                return Json(JsonConvert.SerializeObject(zones));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public IActionResult LoadDataCity()
        {
            try
            {
                List<CityDetails> cities = new List<CityDetails>();

                cities = (from c in _skodaContext.City
                          join z in _skodaContext.Zone
                          on c.ZoneId equals z.Id
                          select new CityDetails()
                          {
                              Pincode = c.Pincode,
                              Id = c.Id,
                              Name = c.Name,
                              ZoneId = z.Id,
                              ZoneName = z.Name
                          }).ToList();
                return Json(JsonConvert.SerializeObject(cities));

            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public IActionResult LoadDataState()
        {
            try
            {
                List<StateDetails> cities = new List<StateDetails>();

                cities = (from c in _skodaContext.State
                          join z in _skodaContext.Zone
                          on c.ZoneId equals z.Id
                          select new StateDetails()
                          {
                              Id = c.Id,
                              Name = c.Name,
                              ZoneId = z.Id,
                              ZoneName = z.Name
                          }).ToList();
                return Json(JsonConvert.SerializeObject(cities));

            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public IActionResult FillCity(int id)
        {
            try
            {
                List<CityDetails> cities = new List<CityDetails>();

                cities = (from c in _skodaContext.City
                          join z in _skodaContext.Zone
                          on c.ZoneId equals z.Id
                          where z.Id == id && c.IsOpen == true
                          select new CityDetails()
                          {
                              Id = c.Id,
                              Name = c.Name
                          }).ToList();
                return Json(cities);

            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public IActionResult LoadDataRole()
        {
            try
            {

                List<RoleDetails> roles = new List<RoleDetails>();
                roles = _skodaContext.Roles.Where(x => !x.IsDeleted).Select(e => new RoleDetails
                {
                    Name = e.Name,
                    RoleId = e.RoleId
                }).ToList();

                return Json(JsonConvert.SerializeObject(roles));

            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult Profile()
        {
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
                return RedirectToAction("Login", "Admin");

            UserDetails uInfo = new UserDetails();
            UserProfile profile = new UserProfile();
            if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
            {
                uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }
            ViewBag.UserName = uInfo.Username;
            ViewBag.Role = uInfo.Role;
            if (uInfo != null)
            {
                profile = (from e in _skodaContext.Users
                           where e.IsDeleted == false
                           select new UserProfile
                           {
                               FullName = e.Fullname,
                               Username = e.Username,
                               UserId = e.UserId,
                               RoleId = e.RoleId
                           }).Where(x => x.UserId == uInfo.UserId).FirstOrDefault();
                ViewBag.UserName = uInfo.Username;


                var roles = (from role in _skodaContext.Roles.Where(r => !r.IsDeleted)
                             select new SelectListItem()
                             {
                                 Text = role.Name,
                                 Value = role.RoleId.ToString(),
                                 Selected = profile.RoleId > 0 ? profile.RoleId == role.RoleId : false
                             }).ToList();

                roles.Insert(0, new SelectListItem()
                {
                    Text = "----Select----",
                    Value = string.Empty,
                });
                profile.Roles = roles;
            }
            return View(profile);
        }
        public ActionResult InvokeNotification(long id, long delearId)
        {
            var notification = _skodaContext.Notifications.Where(n => n.Id == id).FirstOrDefault();
            notification.IsRead = true;
            _skodaContext.Update(notification);
            _skodaContext.SaveChanges();

            return RedirectToAction("ViewDetails", "Admin", new { id = delearId });
        }
        private string getCity(int Id)
        {
            return _skodaContext.City.Where(x => x.Id == Id).Select(y => y.Name).FirstOrDefault();
        }
        private void StageUpdate(string EnquiryId, UserDetails uInfo, string comment, int stageId, string stagDescr, string stagOrg)
        {
            string Fullname = uInfo.Fullname;
            string strEmail = string.Empty, strBody, strSubject;
            var delear = _skodaContext.Delear_Enquiry.Where(x => x.Enquiry_id == EnquiryId).FirstOrDefault();
            int delearId = delear.Dealer_ID;
            strEmail = delear.Email;
            var enq = _skodaContext.Enquiry_Status.Where(x => x.Enquiry_Id == EnquiryId).FirstOrDefault();
            if (stageId != 7)
            {
                enq.Stage_id = stageId;
                enq.Created_Date = DateTime.Now;
                enq.User_Id = uInfo.UserId;
                _skodaContext.Enquiry_Status.Update(enq);
                _skodaContext.SaveChanges();
                delear.Status_ID = Convert.ToInt32(Status.Approved);
                _skodaContext.Delear_Enquiry.Update(delear);

                DealerActivity activity = new DealerActivity();
                activity.Created_date = DateTime.Now;
                activity.Delear_ID = delearId;
                if (stagOrg == "Final Evaluation")
                {
                    activity.Description = Fullname + " has Approved at " + stagOrg + " stage.";
                    activity.Comment = Fullname + " has Approved at " + stagOrg + " stage.";

                }
                else
                {
                    activity.Description = Fullname + " has Approved at " + stagOrg + " stage and Moved to " + stagDescr + " stage.";
                    activity.Comment = Fullname + " has Approved at " + stagOrg + " stage and Moved to " + stagDescr + " stage.";
                }
                _skodaContext.DealerActivity.Add(activity);
                _skodaContext.SaveChanges();
            }

            var emails = _skodaContext.Email_Template.Where(x => x.Template_ID == "Approved_Status_MovedStage_Delear").FirstOrDefault();
            var Adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "Approved_Status_MovedStage_Network_Team").FirstOrDefault();
            strSubject = emails.Subject.Replace("#city#", getCity(delear.City_Id)).Replace("#stage#", stagOrg).Replace("#Nstage#", stagDescr);
            if (stagOrg == "Final Evaluation")
            {
                strBody = emails.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("<strong>{ “Requisite Documents” }</strong> stage and moved to <strong>{ “Pre Due Dilligence” }</strong> stage", stagDescr + " stage")
               .Replace("{ Network user full name }", Fullname).Replace("#link", AppUrl + "Delear/login").Replace("{ comment }", comment);
                strSubject = emails.Subject.Replace("#city#", getCity(delear.City_Id)).Replace("#stage# and moved to #Nstage#", stagDescr + " stage.");
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, delear.Email, strBody, strSubject, true, null);
                strBody = Adminemail.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("<strong>{ “Requisite Documents” }</strong> stage and moved to <strong>{ “Pre Due Dilligence” }</strong> stage", stagDescr + " stage")
                     .Replace("{ Network user full name }", Fullname).Replace("#link", AppUrl + "Admin/login").Replace("{ comment }", comment);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, uInfo.Email, strBody, strSubject, true, null);

            }
            else if (stagOrg == "Requisite document")
            {
                var emailM = _skodaContext.Email_Template.Where(x => x.Template_ID == "Approved_Status_MovedStage_MasterDocument").FirstOrDefault();
                strSubject = emailM.Subject.Replace("#city#", getCity(delear.City_Id));
                StringBuilder HStable = new StringBuilder();
                HStable.Append("<table class='skd-prf-tbl' cellspacing='0' style='border-collapse:collapse; width:100%'>");
                HStable.Append("<tr><th style = 'background:#e9f1e8; text-align:center; width:50%'>File Name</th><th style ='background:#e9f1e8; text-align:center'> Download Links </th></tr>");
                var master = _skodaContext.Admin_FileUpload.ToList();
                foreach (var vs in master)
                {
                    HStable.Append("<tr><td style='text-align:center;width:50%'>" + vs.FileName + "</td>");
                    HStable.Append("<td style='text-align:center;' ><a href='" + AppUrl + vs.FilePath + "' >" + vs.FileName + "</a> </td></tr>");
                }
                HStable.Append("</table>");
                strBody = emailM.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("#enqID#", delear.Enquiry_id)
                    .Replace("#MasterLink#", HStable.ToString()).Replace("#PortalLink#", AppUrl + "Delear/login").Replace("{ “Requisite Documents” }", stagOrg).Replace("{ “Pre Due Dilligence” }", stagDescr);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, delear.Email, strBody, strSubject, true, null);
            }
            else
            {
                strBody = emails.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("{ “Requisite Documents” }", stagOrg).Replace("{ “Pre Due Dilligence” }", stagDescr)
                   .Replace("{ Network user full name }", Fullname).Replace("#link", AppUrl + "Delear/login").Replace("{ comment }", comment);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, delear.Email, strBody, strSubject, true, null);
                strBody = Adminemail.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("{ “Requisite Documents” }", stagOrg).Replace("{ “Pre Due Dilligence” }", stagDescr)
                     .Replace("{ Network user full name }", Fullname).Replace("#link", AppUrl + "Admin/login").Replace("{ comment }", comment);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, uInfo.Email, strBody, strSubject, true, null);

            }
        }
        [HttpPost]
        public IActionResult StatusUpdate(string status, string EnquiryId, string comment)
        {
            try
            {
                UserDetails uInfos = null;
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfos = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                int StatusId = 0, stageId = 0;
                string strBody, strSubject;
                string strEmail = string.Empty, stagDescr = string.Empty, stagOrg = string.Empty;
                var enq = _skodaContext.Enquiry_Status.Where(x => x.Enquiry_Id == EnquiryId).FirstOrDefault();
                var delear = _skodaContext.Delear_Enquiry.Where(x => x.Enquiry_id == EnquiryId).FirstOrDefault();
                int delearId = delear.Dealer_ID;
                stageId = enq.Stage_id + 1;
                if (stageId == 2)
                {
                    stagDescr = "Pre Due Diligence";
                    stagOrg = "Requisite document";
                }

                else if (stageId == 3)
                {
                    stagDescr = "Due Diligence";
                    stagOrg = "Pre Due Diligence";
                }
                else if (stageId == 4)
                {
                    stagDescr = "Presentation & interview";
                    stagOrg = "Due Diligence";
                }
                else if (stageId == 5)
                {
                    stagDescr = "Final Evaluation";
                    stagOrg = "Presentation & interview";
                }
                else if (stageId == 6)
                {
                    stagDescr = "Final Evaluation";
                    stagOrg = "Final Evaluation";
                }
                if (status == "Open")
                {
                    StatusId = Convert.ToInt32(Status.Open);
                }
                if (status == "Rejected")
                {
                    StatusId = Convert.ToInt32(Status.Rejected);
                    var emails = _skodaContext.Email_Template.Where(x => x.Template_ID == "rejected_Delear").FirstOrDefault();
                    var Adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "rejected_Network_Team").FirstOrDefault();
                    string CityName = getCity(delear.City_Id);
                    strBody = emails.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("{Enq.ID}", EnquiryId).Replace("“Requisite Documents”", stagOrg).Replace("(Reason for Rejection)", comment)
                        .Replace("{ Network user’s full name }", uInfos.Fullname).Replace("#link", AppUrl + "Delear/login");
                    strSubject = emails.Subject.Replace("#city#", CityName).Replace("#stage#", stagDescr);
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, delear.Email, strBody, strSubject, true, null);

                    strBody = Adminemail.Body.Replace("{ Dealer User Full Name }", delear.Fullname)
                        .Replace("{Enq.ID}", EnquiryId).Replace("{ Network user’s full name }", uInfos.Fullname).Replace("“Requisite Documents”", stagOrg).Replace("(Reason for Rejection)", comment)
                        .Replace("#link", AppUrl + "Admin/login").Replace("#1", EnquiryId).Replace("#2", delear.Fullname).Replace("#3", delear.Email)
                        .Replace("#4", delear.Mobile_No).Replace("#5", CityName).Replace("#6", delear.Bussiness_Type).Replace("#7", delear.Register_bussinessName).Replace("#8", delear.Brand_name)
                        .Replace("#9", delear.About_businessExp).Replace("#Source", delear.Source).Replace("#How", delear.How_Know); ;
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, uInfos.Email, strBody, strSubject, true, null);
                }
                if (status == "Approved")
                {
                    StatusId = Convert.ToInt32(Status.Approved);
                    StageUpdate(EnquiryId, uInfos, comment, stageId, stagDescr, stagOrg);
                }



                enq.Status_id = StatusId;
                enq.User_Id = uInfos.UserId;
                _skodaContext.Enquiry_Status.Update(enq);
                _skodaContext.SaveChanges();

                delear.Status_ID = StatusId;
                _skodaContext.Delear_Enquiry.Update(delear);
                _skodaContext.SaveChanges();
                string statusDescr = string.Empty;
                if (StatusId == 1)
                    statusDescr = "Open";
                else if (StatusId == 2)
                    statusDescr = "OnHold";
                else if (StatusId == 3)
                    statusDescr = "Rejected";
                else if (StatusId == 4)
                    statusDescr = "Approved";

                DealerActivity activity = new DealerActivity();
                activity.Created_date = DateTime.Now;
                activity.Delear_ID = delearId;
                activity.Description = uInfos.Fullname + " has " + statusDescr + " at " + stagOrg;
                activity.Comment = uInfos.Fullname + " has " + statusDescr + " at " + stagOrg;
                _skodaContext.DealerActivity.Add(activity);

                activity = new DealerActivity();
                activity.Created_date = DateTime.Now;
                activity.Delear_ID = delearId;
                activity.Description = uInfos.Fullname + " has Commented: " + comment;
                activity.Comment = uInfos.Fullname + " has Commented: " + comment;
                _skodaContext.DealerActivity.Add(activity);


                AuditLog audit = new AuditLog();
                audit.CreatedOn = DateTime.Now;
                audit.CurrentStatus_id = StatusId - 1;
                audit.Admin_id = uInfos.UserId;
                audit.Delear_id = delearId;
                audit.Description = uInfos.Fullname + " has " + statusDescr + " at " + stagOrg;
                audit.UpdatedStatus_id = StatusId;
                _skodaContext.AuditLog.Add(audit);
                _skodaContext.SaveChanges();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            var msg = "success";
            return Json(msg);

        }
        [HttpPost]
        public IActionResult rejectallEnquiry(string[] EnquiryId)
        {
            try
            {
                UserDetails uInfos = null;
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfos = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                int StatusId = 0;
                var emails = _skodaContext.Email_Template.Where(x => x.Template_ID == "rejected_Delear").FirstOrDefault();
                var Adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "rejected_Network_Team").FirstOrDefault();
                foreach (var item in EnquiryId)
                {
                    string strSubject = string.Empty, strBody = string.Empty, strEmail = string.Empty, stagDescr = string.Empty, stagOrg = string.Empty, statusDescr = string.Empty;
                    StatusId = Convert.ToInt32(Status.Rejected);
                    var delear = _skodaContext.Delear_Enquiry.Where(x => x.Enquiry_id == item).FirstOrDefault();
                    int delearId = delear.Dealer_ID;
                    var enq = _skodaContext.Enquiry_Status.Where(x => x.Enquiry_Id == item).FirstOrDefault();
                    enq.Status_id = StatusId;
                    enq.User_Id = uInfos.UserId;
                    _skodaContext.Enquiry_Status.Update(enq);
                    _skodaContext.SaveChanges();

                    delear.Status_ID = StatusId;
                    _skodaContext.Delear_Enquiry.Update(delear);
                    _skodaContext.SaveChanges();
                    if (StatusId == 1)
                        statusDescr = "Open";
                    else if (StatusId == 2)
                        statusDescr = "OnHold";
                    else if (StatusId == 3)
                        statusDescr = "Rejected";
                    else if (StatusId == 4)
                        statusDescr = "Approved";
                    DealerActivity activity = new DealerActivity();
                    activity.Created_date = DateTime.Now;
                    activity.Delear_ID = delearId;
                    activity.Description = uInfos.Fullname + " has " + statusDescr + " all enquiry.";
                    activity.Comment = uInfos.Fullname + " has " + statusDescr + " all enquiry.";

                    _skodaContext.DealerActivity.Add(activity);


                    AuditLog audit = new AuditLog();
                    audit.CreatedOn = DateTime.Now;
                    audit.CurrentStatus_id = StatusId - 1;
                    audit.Admin_id = uInfos.UserId;
                    audit.Delear_id = delearId;
                    audit.Description = uInfos.Fullname + " has " + statusDescr + " all enquiry.";
                    audit.UpdatedStatus_id = StatusId;
                    _skodaContext.AuditLog.Add(audit);
                    _skodaContext.SaveChanges();
                    strBody = emails.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("{Enq.ID}", item).Replace("“Requisite Documents”", stagOrg).Replace("(Reason for Rejection)", "rejected enquiry")
                        .Replace("{ Network user’s full name }", uInfos.Fullname).Replace("#link", AppUrl + "Delear/login");
                    strSubject = emails.Subject.Replace("#city#", getCity(delear.City_Id)).Replace("#stage#", stagDescr);
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, delear.Email, strBody, strSubject, true, null);
                    strBody = Adminemail.Body.Replace("{ Dealer User Full Name }", delear.Fullname)
                        .Replace("{Enq.ID}", item).Replace("{ Network user’s full name }", uInfos.Fullname).Replace("“Requisite Documents”", stagOrg).Replace("(Reason for Rejection)", "rejected enquiry")
                        .Replace("#link", AppUrl + "Admin/login");
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, uInfos.Email, strBody, strSubject, true, null);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            var msg = "success";
            return Json(msg);

        }

        [HttpPost]
        public IActionResult CommentsUpdate(string enquiryId, string comment, string[] fileId)
        {
            try
            {

                var delear = _skodaContext.Delear_Enquiry.Where(x => x.Enquiry_id == enquiryId).FirstOrDefault();
                int delearId = delear.Dealer_ID;

                DealerActivity activity = new DealerActivity();
                activity.Created_date = DateTime.Now;
                activity.Delear_ID = delearId;
                activity.Description = "Admin Comment: " + comment;
                activity.Comment = comment;
                _skodaContext.DealerActivity.Add(activity);
                _skodaContext.SaveChanges();
                SendEmails(delear.Email, delear.Fullname, fileId, comment);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            var msg = "success";
            return Json(msg);

        }
        private void SendEmails(string email, string name, string[] fileId, string comment)
        {
            string strBody, strSubject, adminstrBody, adminstrSubject;
            bool blnFileAttached = false;
            StringBuilder HStable = new StringBuilder();
            HStable.Append("<table class='skd-prf-tbl' cellspacing='0' style='border-collapse:collapse; width:100%'>");
            HStable.Append("<tr><th style = 'background:#e9f1e8; text-align:center; width:50%'>File Name</th><th style ='background:#e9f1e8; text-align:center'> Download Links </th></tr>");
            foreach (var f in fileId)
            {
                var vs = _skodaContext.Admin_FileUpload.Where(x => x.Id == Convert.ToInt32(f.ToString())).FirstOrDefault();
                HStable.Append("<tr><td style='text-align:center;width:50%'>" + vs.FileName + "</td>");
                HStable.Append("<td style='text-align:center;' ><a href='" + AppUrl + vs.FilePath + "' >" + vs.FileName + "</a> </td></tr>");
                blnFileAttached = true;
            }
            HStable.Append("</table>");
            UserDetails uInfos = null;
            if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
            {
                uInfos = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }
            if (blnFileAttached == true)
            {

                var demail = _skodaContext.Email_Template.Where(x => x.Template_ID == "admin_filesent").FirstOrDefault();
                strBody = demail.Body;
                strSubject = demail.Subject.Replace("#city#", uInfos.City);
                var adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "Admin_FileSentWithComment_Network_Team").FirstOrDefault();
                adminstrBody = adminemail.Body;
                adminstrSubject = adminemail.Subject.Replace("#city#", uInfos.City);
                strBody = strBody.Replace("{ Dealer User Full Name }", name)
                    .Replace("{comment}", comment)
                    .Replace("{document}", HStable.ToString())
                    .Replace("#link", AppUrl + "Delear/login");
                adminstrBody = adminstrBody.Replace("{ Dealer User }", name).Replace("{comment}", comment)
                    .Replace("{document}", HStable.ToString()).Replace("{ Network User }", uInfos.Fullname)
                    .Replace("#link", AppUrl + "Admin/login");
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, email, strBody, strSubject, true, null);
                string AdminEmail = _skodaContext.Users.Where(x => x.UserId == x.UserId).Select(x => x.Email).FirstOrDefault();
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, AdminEmail, adminstrBody, adminstrSubject, true, null);
            }
            else
            {

                var demail = _skodaContext.Email_Template.Where(x => x.Template_ID == "admin_comment").FirstOrDefault();
                strBody = demail.Body.Replace("{ Dealer User Full Name }", name)
                    .Replace("{comment}", comment).Replace("#link", AppUrl + "Delear/login");
                strSubject = demail.Subject.Replace("#city#", uInfos.City);
                var adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "Admin_Comment_Network_Team").FirstOrDefault();
                adminstrBody = adminemail.Body.Replace("{ Network User }", uInfos.Fullname)
               .Replace("{ Dealer User }", name).Replace("{comment}", comment)
               .Replace("{Network portal link}", AppUrl + "Admin/login");
                adminstrSubject = adminemail.Subject.Replace("#city#", uInfos.City);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, email, strBody, strSubject, true, null);
                string AdminEmail = _skodaContext.Users.Where(x => x.UserId == uInfos.UserId).Select(x => x.Email).FirstOrDefault();
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, AdminEmail, adminstrBody, adminstrSubject, true, null);

            }
        }
        [HttpPost]
        public IActionResult Profile(UserProfile model)
        {
            UserDetails uInfo = null;
            if (ModelState.IsValid)
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }


                var roles = (from role in _skodaContext.Roles.Where(r => !r.IsDeleted)
                             select new SelectListItem()
                             {
                                 Text = role.Name,
                                 Value = role.RoleId.ToString(),
                                 Selected = model.RoleId > 0 ? model.RoleId == role.RoleId : false
                             }).ToList();
                //string strBody, strSubject, adminstrBody, adminstrSubject;
                //var email = _skodaContext.Email_Template.Where(x => x.Template_ID == "admin_comment").FirstOrDefault();
                //strBody = email.Body;
                //strSubject = email.Subject.Replace("#city#", getCity(delear.City_Id)).Replace("#stage#", stagDescr);
                //var adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "Admin_Comment_Network_Team").FirstOrDefault();
                //adminstrBody = adminemail.Body;
                //adminstrSubject = adminemail.Subject.Replace("#city#", uInfo.City);
                //EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, uInfo.Email, strBody, strSubject, true, null);
                //string AdminEmail = _skodaContext.Users.Where(x => x.Username == "admin").Select(x => x.Email).FirstOrDefault();
                //EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, AdminEmail, adminstrBody, adminstrSubject, true, null);

                roles.Insert(0, new SelectListItem()
                {
                    Text = "----Select----",
                    Value = string.Empty,
                });
                model.Roles = roles;

                var emp = _skodaContext.Users.Where(x => x.UserId == uInfo.UserId).FirstOrDefault();
                emp.Username = model.Username;
                emp.Fullname = model.FullName;
                emp.UserId = model.UserId;
                emp.RoleId = model.RoleId;
                _skodaContext.Update(emp);
                _skodaContext.SaveChanges();
            }
            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public IActionResult Create(UserDetails model)
        {
            if (ModelState.IsValid)
            {
                if (model.UserId == 0)
                {
                    var user = _skodaContext.Users.Where(x => EF.Functions.Like(model.Username, x.Username)).FirstOrDefault();
                    if (user == null)
                    {
                        var emp = new Users()
                        {
                            Username = model.Username,
                            Fullname = model.Fullname,
                            UserId = model.UserId,
                            CreatedOn = DateTime.Now,
                            IsDeleted = false,
                            RoleId = model.RoleId,
                            CityId = model.CityId,
                            Mobile = model.Mobile,
                            Email = model.Email,
                            Password = EncryptDecryptUtils.Encrypt(PhraseKey, model.Password)

                        };

                        var names = model.Fullname.Replace("  ", " ").Trim().ToUpper().Split(" ");

                        _skodaContext.Users.Add(emp);
                        _skodaContext.SaveChanges();

                        try
                        {
                            EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, _smtpSettings.FromAddress, "Hello " + model.Fullname + ",<br/><br/>" + "Your User has been created. Please use below credentials to login.<br/><br/>Username:" + model.Username + "<br/>Password:" + model.Password + " <br/><br/>Thanks,<br/>Skoda DIAS Team", "Your account has been created", true, null);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "Error Sending Email to", _smtpSettings.FromAddress, model.Username);
                        }
                    }
                    else
                    {
                        model.ErrorMessage = "Username already exists.";

                        var cities = (from city in _skodaContext.City
                                      orderby city.Name ascending
                                      select new SelectListItem()
                                      {
                                          Text = city.Name,
                                          Value = city.Id.ToString(),
                                          Selected = model.CityId > 0 ? model.CityId == city.Id : false
                                      }).ToList();

                        cities.Insert(0, new SelectListItem()
                        {
                            Text = "----Select----",
                            Value = string.Empty,
                        });
                        model.Cities = cities;
                        var roles = (from role in _skodaContext.Roles.Where(r => !r.IsDeleted)
                                     select new SelectListItem()
                                     {
                                         Text = role.Name,
                                         Value = role.RoleId.ToString(),
                                         Selected = model.RoleId > 0 ? model.RoleId == role.RoleId : false
                                     }).ToList();

                        roles.Insert(0, new SelectListItem()
                        {
                            Text = "----Select----",
                            Value = string.Empty,
                        });
                        model.Roles = roles;
                        return View(model);
                    }
                }
                else
                {
                    var emp = _skodaContext.Users.Where(x => x.UserId == model.UserId).FirstOrDefault();
                    emp.Fullname = model.Fullname;
                    emp.UserId = model.UserId;
                    emp.IsDeleted = false;
                    emp.RoleId = model.RoleId;
                    emp.CityId = model.CityId;
                    var names = model.Fullname.Replace("  ", " ").Trim().ToUpper().Split(" ");

                    if (!string.IsNullOrEmpty(model.Password))
                        emp.Password = EncryptDecryptUtils.Encrypt(PhraseKey, model.Password);

                    _skodaContext.Update(emp);
                    _skodaContext.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
            {
                var roles = (from role in _skodaContext.Roles.Where(r => !r.IsDeleted)
                             select new SelectListItem()
                             {
                                 Text = role.Name,
                                 Value = role.RoleId.ToString(),
                                 Selected = model.RoleId > 0 ? model.RoleId == role.RoleId : false
                             }).ToList();

                roles.Insert(0, new SelectListItem()
                {
                    Text = "----Select----",
                    Value = string.Empty,
                });
                model.Roles = roles;
                var cities = (from city in _skodaContext.City
                              orderby city.Name ascending
                              select new SelectListItem()
                              {
                                  Text = city.Name,
                                  Value = city.Id.ToString(),
                                  Selected = model.CityId > 0 ? model.CityId == city.Id : false

                              }).ToList();

                cities.Insert(0, new SelectListItem()
                {
                    Text = "----Select----",
                    Value = string.Empty,
                });
                model.Cities = cities;
                return View(model);
            }

        }

        public IActionResult Create(int id)
        {
            UserDetails empModel = new UserDetails();
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            if (id > 0)
            {
                var emp = _skodaContext.Users.Where(x => x.UserId == id).FirstOrDefault();
                empModel.Username = emp.Username;
                empModel.Fullname = emp.Fullname;
                empModel.UserId = emp.UserId;
                empModel.RoleId = emp.RoleId;
                empModel.CityId = emp.CityId;
                empModel.Mobile = emp.Mobile;
                empModel.Email = emp.Email;
                empModel.Password = emp.Password;
            }


            var roles = (from role in _skodaContext.Roles.Where(r => !r.IsDeleted)
                         select new SelectListItem()
                         {
                             Text = role.Name,
                             Value = role.RoleId.ToString(),
                             Selected = empModel.RoleId > 0 ? empModel.RoleId == role.RoleId : false
                         }).ToList();

            roles.Insert(0, new SelectListItem()
            {
                Text = "----Select----",
                Value = string.Empty,
            });

            empModel.Roles = roles;

            var cities = (from city in _skodaContext.City
                          orderby city.Name ascending
                          select new SelectListItem()
                          {
                              Text = city.Name,
                              Value = city.Id.ToString(),
                              Selected = empModel.CityId > 0 ? empModel.CityId == city.Id : false
                          }).ToList();

            cities.Insert(0, new SelectListItem()
            {
                Text = "----Select----",
                Value = string.Empty,
            });

            empModel.Cities = cities;

            return View(empModel);
        }
        [HttpPost]
        public JsonResult FetchNotification()
        {
            List<Notifications> notifications = new List<Notifications>();
            notifications = _skodaContext.Notifications.OrderByDescending(n => n.Id).Where(n => n.IsRead == false).ToList();
            return new JsonResult(new
            {
                Result = "success",
                data = notifications
            });
        }
        public IActionResult Delete(long id)
        {
            var singleRec = _skodaContext.Users.FirstOrDefault(x => x.UserId == id);// object your want to delete
            if (singleRec != null)
            {
                singleRec.IsDeleted = true;
                _skodaContext.Update(singleRec);
                _skodaContext.SaveChanges();

                return new JsonResult(new { result = "success" });
            }
            return new JsonResult(new { result = "failure" });
        }
        public IActionResult DeleteDocument(long id)
        {
            var singleRec = _skodaContext.Admin_FileUpload.FirstOrDefault(x => x.Id == id);
            if (singleRec != null)
            {
                _skodaContext.Remove(singleRec);
                _skodaContext.SaveChanges();

                return new JsonResult(new { result = "success" });
            }
            return new JsonResult(new { result = "failure" });
        }

        public IActionResult RoleCreate(int id)
        {
            RoleDetails rModel = new RoleDetails();

            if (id > 0)
            {
                var r = _skodaContext.Roles.Where(x => x.RoleId == id).FirstOrDefault();
                rModel.Name = r.Name;
                rModel.RoleId = r.RoleId;
            }
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View(rModel);
        }

        [HttpPost]
        public IActionResult RoleCreate(RoleDetails model)
        {
            if (ModelState.IsValid)
            {
                if (model.RoleId == 0)
                {
                    var role = _skodaContext.Roles.Where(x => EF.Functions.Like(model.Name, x.Name)).FirstOrDefault();
                    if (role == null)
                    {
                        var rol = new Roles()
                        {
                            Name = model.Name,
                            RoleId = model.RoleId
                        };
                        _skodaContext.Roles.Add(rol);
                        _skodaContext.SaveChanges();
                    }
                    else
                    {
                        model.ErrorMessage = "role already exists.";

                        return View(model);
                    }
                }
                else
                {
                    var r = _skodaContext.Roles.Where(x => x.RoleId == model.RoleId).FirstOrDefault();
                    r.Name = model.Name;
                    r.RoleId = model.RoleId;
                    _skodaContext.Update(r);
                    _skodaContext.SaveChanges();
                }

                return RedirectToAction("RoleList");
            }
            else
                return View(model);
        }

        [HttpPost]
        public IActionResult CityCreate(CityDetails model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var role = _skodaContext.City.Where(x => EF.Functions.Like(model.Name, x.Name) && x.IsOpen == true).FirstOrDefault();
                    if (role == null)
                    {
                        var rol = new City()
                        {
                            Name = model.Name,
                            ZoneId = model.ZoneId,
                            Id = model.Id,
                            Pincode = model.Pincode,
                            StateId = model.StateId,
                            IsOpen = model.IsOpen,
                        };
                        _skodaContext.City.Add(rol);
                        _skodaContext.SaveChanges();
                    }
                }
                else
                {
                    var r = _skodaContext.City.Where(x => x.Id == model.Id).FirstOrDefault();
                    r.Name = model.Name;
                    r.ZoneId = model.ZoneId;
                    r.Id = model.Id;
                    r.IsOpen = model.IsOpen;
                    r.StateId = model.StateId;
                    r.Pincode = model.Pincode;
                    _skodaContext.Update(r);
                    _skodaContext.SaveChanges();
                }
                return RedirectToAction("CityList");
            }
            else
            {
                var zones = (from c in _skodaContext.Zone
                             select new SelectListItem()
                             {
                                 Text = c.Name,
                                 Value = c.Id.ToString(),
                                 Selected = model.Id > 0 ? model.Id == model.Id : false
                             }).ToList();

                zones.Insert(0, new SelectListItem()
                {
                    Text = "----Select----",
                    Value = string.Empty,
                });
                model.Zones = zones;
                return View(model);
            }

        }

        [HttpPost]
        public IActionResult StateCreate(StateDetails model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var role = _skodaContext.State.Where(x => EF.Functions.Like(model.Name, x.Name)).FirstOrDefault();
                    if (role == null)
                    {
                        var rol = new State()
                        {
                            Name = model.Name,
                            ZoneId = model.ZoneId,
                            Id = model.Id,
                        };
                        _skodaContext.State.Add(rol);
                        _skodaContext.SaveChanges();
                    }
                }
                else
                {
                    var r = _skodaContext.State.Where(x => x.Id == model.Id).FirstOrDefault();
                    r.Name = model.Name;
                    r.ZoneId = model.ZoneId;
                    r.Id = model.Id;
                    _skodaContext.Update(r);
                    _skodaContext.SaveChanges();
                }
                return RedirectToAction("StateList");
            }
            else
            {
                var zones = (from c in _skodaContext.Zone
                             select new SelectListItem()
                             {
                                 Text = c.Name,
                                 Value = c.Id.ToString(),
                                 Selected = model.Id > 0 ? model.Id == model.Id : false
                             }).ToList();

                zones.Insert(0, new SelectListItem()
                {
                    Text = "----Select----",
                    Value = string.Empty,
                });
                model.Zones = zones;
                return View(model);
            }

        }
        [AllowAnonymous]
        public IActionResult Enquirystatus(int s, int id)
        {
            EnquiryStatusUpdate model = new EnquiryStatusUpdate();
            var enq1 = _skodaContext.Enquiry_Status.Where(x => x.Delear_Id == id).FirstOrDefault();
            if (enq1.Status_id == 2)
                model.Updated = 1;
            else
                model.Updated = 0;
            model.Status = s;
            return View(model);
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Enquirystatus(EnquiryStatusUpdate enq)
        {

            int StatusId = 0, stageId = 0;
            string strBody, strSubject;
            string strEmail = string.Empty, stagDescr = string.Empty, stagOrg = string.Empty;
            var enq1 = _skodaContext.Enquiry_Status.Where(x => x.Delear_Id == enq.Id).FirstOrDefault();
            var delear = _skodaContext.Delear_Enquiry.Where(x => x.Dealer_ID == enq.Id).FirstOrDefault();
            var user = _skodaContext.Users.Where(x => x.Username == "admin").FirstOrDefault();
            int delearId = delear.Dealer_ID;
            stageId = enq1.Stage_id + 1;
            if (stageId == 2)
            {
                stagDescr = "Pre Due Diligence";
                stagOrg = "Requisite document";
            }

            else if (stageId == 3)
            {
                stagDescr = "Due Diligence";
                stagOrg = "Pre Due Diligence";
            }
            else if (stageId == 4)
            {
                stagDescr = "Presentation & interview";
                stagOrg = "Due Diligence";
            }
            else if (stageId == 5)
            {
                stagDescr = "Final Evaluation";
                stagOrg = "Presentation & interview";
            }
            else if (stageId == 6)
            {
                stagDescr = "Final Evaluation";
                stagOrg = "Final Evaluation";
            }
            if (enq.Status == 2)
            {
                StatusId = Convert.ToInt32(Status.Rejected);
                var emails = _skodaContext.Email_Template.Where(x => x.Template_ID == "rejected_Delear").FirstOrDefault();
                var Adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "rejected_Network_Team").FirstOrDefault();
                strBody = emails.Body.Replace("{ Dealer User Full Name }", delear.Fullname).Replace("{Enq.ID}", enq1.Enquiry_Id).Replace("“Requisite Documents”", stagOrg).Replace("(Reason for Rejection)", enq.Comment)
                    .Replace("{ Network user’s full name }", "Admin").Replace("#link", AppUrl + "Delear/login");
                strSubject = emails.Subject.Replace("#city#", getCity(delear.City_Id)).Replace("#stage#", stagDescr);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, delear.Email, strBody, strSubject, true, null);
                strBody = Adminemail.Body.Replace("{ Dealer User Full Name }", delear.Fullname)
                    .Replace("{Enq.ID}", enq1.Enquiry_Id).Replace("{ Network user’s full name }", "Admin").Replace("“Requisite Documents”", stagOrg).Replace("(Reason for Rejection)", enq.Comment)
                    .Replace("#link", AppUrl + "Admin/login");
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, user.Email, strBody, strSubject, true, null);
            }
            UserDetails uInfos = new UserDetails();
            uInfos.UserId = user.UserId;
            uInfos.Fullname = user.Fullname;
            uInfos.Email = user.Email;
            if (enq.Status == 1)
            {
                StatusId = Convert.ToInt32(Status.Approved);
                StageUpdate(enq1.Enquiry_Id, uInfos, enq.Comment, stageId, stagDescr, stagOrg);
            }
            enq1.Status_id = StatusId;
            enq1.User_Id = uInfos.UserId;
            _skodaContext.Enquiry_Status.Update(enq1);
            _skodaContext.SaveChanges();

            delear.Status_ID = StatusId;
            _skodaContext.Delear_Enquiry.Update(delear);
            _skodaContext.SaveChanges();
            string statusDescr = string.Empty;
            if (StatusId == 2)
                statusDescr = "Rejected";
            else if (StatusId == 1)
                statusDescr = "Approved";

            DealerActivity activity = new DealerActivity();
            activity.Created_date = DateTime.Now;
            activity.Delear_ID = delearId;
            activity.Description = "Admin has " + statusDescr + " at " + stagOrg;
            activity.Comment = "Admin has " + statusDescr + " at " + stagOrg;
            _skodaContext.DealerActivity.Add(activity);

            activity = new DealerActivity();
            activity.Created_date = DateTime.Now;
            activity.Delear_ID = delearId;
            activity.Description = "Admin has Commented: " + enq.Comment;
            activity.Comment = "Admin has Commented: " + enq.Comment;
            _skodaContext.DealerActivity.Add(activity);
            _skodaContext.SaveChanges();
            return RedirectToAction("Login", "Admin");
        }
        [AllowAnonymous]
        public IActionResult UploadDocument()
        {
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AdminDealerFileUpload(IList<IFormFile> files, string delerenquiryid)
        {

            string uploadRoot = Path.Combine(_environment.WebRootPath, "uploads");
            string folder = string.Format(uploadRoot + "/{0}/", delerenquiryid);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);   // to create folder

            }
            StringBuilder HStable = new StringBuilder();
            var dInfo = _skodaContext.Delear_Enquiry.Where(x => x.Enquiry_id == delerenquiryid).FirstOrDefault();
            string city = _skodaContext.City.Where(x => x.Id == dInfo.City_Id).Select(x => x.Name).FirstOrDefault();
            if (files.Count > 0)
            {
                HStable.Append("<table class='skd-prf-tbl' cellspacing='0' style='border-collapse:collapse; width:100%'>");
                HStable.Append("<tr><th style = 'background:#e9f1e8; text-align:center;width:50%'>File Name</th><th style ='background:#e9f1e8; text-align:center'> Download Links </th></tr>");
                foreach (IFormFile source in files)
                {
                    var uploads = Path.Combine(_environment.WebRootPath, folder);
                    string filename = ContentDispositionHeaderValue.Parse(source.ContentDisposition).FileName.Trim('"');

                    filename = this.EnsureCorrectFilename(filename);
                    UploadDocument uploadDocument = new UploadDocument();
                    uploadDocument.Upload_Document = filename;
                    uploadDocument.Created_date = DateTime.UtcNow;
                    uploadDocument.Delear_ID = dInfo.Dealer_ID;
                    _skodaContext.UploadDocument.Add(uploadDocument);
                    HStable.Append("<tr><td style='text-align:center; width:50%'>" + filename + "</td>");
                    HStable.Append("<td style='text-align:center;'><a href='" + AppUrl + "/uploads/" + delerenquiryid + "/" + filename + "' >" + filename + "</a></td></tr>");

                    using (FileStream output = System.IO.File.Create(uploads + filename))
                        await source.CopyToAsync(output);
                }
                HStable.Append("</table>");
                string strBody, strSubject, adminstrBody, adminstrSubject;
                var email = _skodaContext.Email_Template.Where(x => x.Template_ID == "uploaddocument_delear").FirstOrDefault();
                strBody = email.Body.Replace("{table}", HStable.ToString()).Replace("{ Dealer User Full Name }", dInfo.Fullname)
                    .Replace("{Dealer Portal link}", AppUrl + "Delear/login");
                strSubject = email.Subject.Replace("#city#", city);
                var adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "Uploaddocument_Network_Team").FirstOrDefault();
                adminstrBody = adminemail.Body.Replace("{table}", HStable.ToString()).Replace("{ Dealer User Fullname }", dInfo.Fullname)
                    .Replace("{Dealer Portal link}", AppUrl + "Admin/login");
                adminstrSubject = adminemail.Subject.Replace("#city#", city);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, dInfo.Email, strBody, strSubject, true, null);
                string AdminEmail = _skodaContext.Users.Where(x => x.Username == "admin").Select(x => x.Email).FirstOrDefault();
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, AdminEmail, adminstrBody, adminstrSubject, true, null);

                DealerActivity activity = new DealerActivity();
                activity.Created_date = DateTime.Now;
                activity.Delear_ID = dInfo.Dealer_ID;
                activity.Description = dInfo.Fullname + " has submitted the Documents.";
                activity.Comment = dInfo.Fullname + " has submitted the Documents.";
                _skodaContext.DealerActivity.Add(activity);

                Notifications notify = new Notifications();
                notify.CreatedDate = DateTime.Now;
                notify.IsDeleted = false;
                notify.IsRead = false;
                notify.Title = "File Uploaded";
                notify.Message = "File Uploaded Successfully By-" + dInfo.Fullname;
                notify.DelearId = dInfo.Dealer_ID;
                notify.UserId = 0;
                _skodaContext.Notifications.Add(notify);

                _skodaContext.SaveChanges();
            }
            return View();
        }
        private string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains("\\"))
                filename = filename.Substring(filename.LastIndexOf("\\") + 1);

            return filename;
        }

        private string GetPathAndFilename(string filename)
        {
            return _environment.WebRootPath + "\\uploads\\" + filename;
        }
        [HttpPost]
        [RequestSizeLimit(500 * 1024 * 1024)]       //unit is bytes => 500Mb
        [RequestFormLimits(MultipartBodyLengthLimit = 500 * 1024 * 1024)]
        public IActionResult DocumentUpload()
        {
            var filelist = HttpContext.Request.Form.Files;


            UserDetails uInfos = null;
            if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
            {
                uInfos = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }
            foreach (var file in filelist)
            {
                string uploadRoot = Path.Combine(_environment.WebRootPath, "uploads");

                string folder = string.Format(uploadRoot + "/{0}/", "documents");
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);   // to create folder

                }

                var uploads = Path.Combine(_environment.WebRootPath, folder);
                Admin_FileUpload uploadDocument = new Admin_FileUpload();
                uploadDocument.FileName = file.FileName;
                uploadDocument.CreatedDate = DateTime.UtcNow;
                uploadDocument.FilePath = "uploads/documents/" + file.FileName;
                uploadDocument.UserId = uInfos.UserId;
                _skodaContext.Admin_FileUpload.Add(uploadDocument);
                _skodaContext.SaveChanges();

                using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                {

                    file.CopyTo(fileStream);
                }
            }
            DealerActivity activity = new DealerActivity();
            activity.Created_date = DateTime.Now;
            activity.Delear_ID = 0;
            activity.Description = "has submitted the Documents.";
            activity.Comment = uInfos.Fullname + "has submitted the Documents.";
            _skodaContext.DealerActivity.Add(activity);

            return RedirectToAction("DocumentIndex");
        }
        [HttpPost]
        public IActionResult ZoneCreate(ZoneDetails model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var role = _skodaContext.Zone.Where(x => EF.Functions.Like(model.Name, x.Name)).FirstOrDefault();
                    if (role == null)
                    {
                        var rol = new Zone()
                        {
                            Name = model.Name,
                            Id = model.Id
                        };
                        _skodaContext.Zone.Add(rol);
                        _skodaContext.SaveChanges();
                    }
                    else
                    {
                        model.ErrorMessage = "zone already exists.";

                        return View(model);
                    }
                }
                else
                {
                    var r = _skodaContext.Zone.Where(x => x.Id == model.Id).FirstOrDefault();
                    r.Name = model.Name;
                    r.Id = model.Id;
                    _skodaContext.Update(r);
                    _skodaContext.SaveChanges();
                }
                return RedirectToAction("ZoneList");
            }
            else
                return View(model);

        }
        public IActionResult ZoneCreate(int id)
        {
            ZoneDetails rzone = new ZoneDetails();
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            if (id > 0)
            {
                var r = _skodaContext.Zone.Where(x => x.Id == id).FirstOrDefault();
                rzone.Name = r.Name;
                rzone.Id = r.Id;
            }

            return View(rzone);
        }

        public IActionResult CityCreate(int id)
        {
            CityDetails rc = new CityDetails();
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            if (id > 0)
            {
                var r = _skodaContext.City.Where(x => x.Id == id).FirstOrDefault();
                rc.Name = r.Name;
                rc.Id = r.Id;
                rc.StateId = Convert.ToInt32(r.StateId);
                rc.Pincode = r.Pincode;
                rc.IsOpen = r.IsOpen;
                rc.ZoneId = r.ZoneId;
                rc.States = _skodaContext.State.Select(z => new SelectListItem { Text = z.Name, Value = z.Id.ToString(), Selected = r.StateId == z.Id }).ToList();
            }

            var zones = (from c in _skodaContext.Zone
                         select new SelectListItem()
                         {
                             Text = c.Name,
                             Value = c.Id.ToString(),
                             Selected = rc.Id > 0 ? rc.Id == rc.Id : false
                         }).ToList();

            zones.Insert(0, new SelectListItem()
            {
                Text = "----Select----",
                Value = string.Empty,
            });
            rc.Zones = zones;

            return View(rc);
        }
        [HttpPost]
        public JsonResult FillState(int ZoneId)
        {
            List<Alias> state = new List<Alias>();

            state = (from x in _skodaContext.State
            .Where(x => x.ZoneId == ZoneId)
                     select new Alias()
                     {
                         Id = x.Id,
                         Name = x.Name
                     }).ToList();
            return Json(state);
        }
        public IActionResult StateCreate(int id)
        {
            StateDetails rc = new StateDetails();
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            if (id > 0)
            {
                var r = _skodaContext.State.Where(x => x.Id == id).FirstOrDefault();
                rc.Name = r.Name;
                rc.Id = r.Id;
                rc.ZoneId = r.ZoneId;
            }

            var zones = (from c in _skodaContext.Zone
                         select new SelectListItem()
                         {
                             Text = c.Name,
                             Value = c.Id.ToString(),
                             Selected = rc.Id > 0 ? rc.Id == rc.Id : false
                         }).ToList();

            zones.Insert(0, new SelectListItem()
            {
                Text = "----Select----",
                Value = string.Empty,
            });
            rc.Zones = zones;

            return View(rc);
        }
        public IActionResult DeleteRole(long Id)
        {
            var singleRec = _skodaContext.Roles.FirstOrDefault(x => x.RoleId == Id);
            if (singleRec != null)
            {
                singleRec.IsDeleted = true;
                _skodaContext.Update(singleRec);
                _skodaContext.SaveChanges();

                return new JsonResult(new { result = "success" });
            }
            return new JsonResult(new { result = "failure" });
        }
        public IActionResult DeleteCity(long Id)
        {
            var singleRec = _skodaContext.City.FirstOrDefault(x => x.Id == Id);
            if (singleRec != null)
            {
                _skodaContext.Remove(singleRec);
                _skodaContext.SaveChanges();

                return new JsonResult(new { result = "success" });
            }
            return new JsonResult(new { result = "failure" });
        }
        public IActionResult DeleteState(long Id)
        {
            var singleRec = _skodaContext.State.FirstOrDefault(x => x.Id == Id);
            if (singleRec != null)
            {
                _skodaContext.Remove(singleRec);
                _skodaContext.SaveChanges();

                return new JsonResult(new { result = "success" });
            }
            return new JsonResult(new { result = "failure" });
        }
        public IActionResult DeleteZone(long Id)
        {
            var singleRec = _skodaContext.Zone.FirstOrDefault(x => x.Id == Id);
            if (singleRec != null)
            {
                _skodaContext.Remove(singleRec);
                _skodaContext.SaveChanges();

                return new JsonResult(new { result = "success" });
            }

            return new JsonResult(new { result = "failure" });
        }
        [HttpPost]
        public ActionResult Details(string enquiryId, int StatusId)
        {
            try
            {
                UserDetails uInfo = null;
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
                var existingEnquiry = _skodaContext.Delear_Enquiry.Where(x => x.Enquiry_id == enquiryId).Select(c => c).FirstOrDefault();
                var stages = _skodaContext.Enquiry_Status.Where(x => x.Enquiry_Id == enquiryId).Select(c => new { c.Stage_id, c.Enquiry_Id, c.Created_Date, c.ModifyDate }).FirstOrDefault();
                var Name = _skodaContext.Stages.Where(x => x.Id == stages.Stage_id).Select(c => new { c.Description }).FirstOrDefault();
                ViewBag.attachDocuments = _skodaContext.UploadDocument.Where(x => x.Delear_ID == existingEnquiry.Dealer_ID).ToList();
                ViewBag.StageName = Name.Description;
                ViewBag.StageId = stages.Stage_id;
                ViewBag.Enquiry_id = existingEnquiry.Enquiry_id;
                ViewBag.fileuploads = _skodaContext.Admin_FileUpload.Select(x => new { Value = x.Id, Text = x.FileName }).ToList();
                var Statusenq = "";

                if (StatusId == 1)
                {
                    Statusenq = "Open";
                }
                else if (StatusId == 2)
                {
                    Statusenq = "OnHold";
                }
                else if (StatusId == 3)
                {
                    Statusenq = "Rejected";
                }
                else if (StatusId == 4)
                {
                    Statusenq = "Approved";
                }
                ActivityDetails dealearActivity = new ActivityDetails();
                dealearActivity.Fullname = existingEnquiry.Fullname;
                dealearActivity.Email = existingEnquiry.Email;
                dealearActivity.Mobile = existingEnquiry.Mobile_No;
                int cityId = existingEnquiry.City_Id;
                dealearActivity.City = Convert.ToString(_skodaContext.City.Where(x => x.Id == cityId).Select(x => x.Name).FirstOrDefault());
                dealearActivity.Pincode = existingEnquiry.Pincode;
                dealearActivity.Bussiness_Type = existingEnquiry.Bussiness_Type;
                dealearActivity.Register_bussinessName = existingEnquiry.Register_bussinessName;
                dealearActivity.Brand_name = existingEnquiry.Brand_name;
                dealearActivity.About_businessExp = existingEnquiry.About_businessExp;
                dealearActivity.source = existingEnquiry.Source;
                dealearActivity.how_know = existingEnquiry.How_Know;
                if (existingEnquiry.BackgroundId == 1)
                    dealearActivity.Background = "Automobile 4 Wheeler";
                else if (existingEnquiry.BackgroundId == 2)
                    dealearActivity.Background = "Automobile Commercial vehicle & Tractors";
                else if (existingEnquiry.BackgroundId == 3)
                    dealearActivity.Background = "Automobile 3 or 2 Wheeler";
                else if (existingEnquiry.BackgroundId == 4)
                    dealearActivity.Background = "Automobile ancillary";
                else if (existingEnquiry.BackgroundId == 5)
                    dealearActivity.Background = "Other Auto related business";
                dealearActivity.OtherLocation = existingEnquiry.OtherLocation;
                dealearActivity.DocumentSubmitted = stages.Created_Date;
                dealearActivity.Modifydate = stages.ModifyDate ?? DateTime.Now;
                dealearActivity.Status = Statusenq;
                dealearActivity.Delear_Id = existingEnquiry.Dealer_ID;
                dealearActivity.activities = new List<Activity>();
                var dealerActivities = _skodaContext.DealerActivity.Where(x => x.Delear_ID == dealearActivity.Delear_Id).OrderByDescending(x => x.Created_date).Take(5).ToList();
                foreach (var a in dealerActivities)
                {
                    Activity activity = new Activity();
                    activity.CreatedDate = a.Created_date.ToString("hh:mm tt") + " | " + a.Created_date.ToString("dd/MM/yyyy");
                    activity.Description = a.Description;
                    dealearActivity.activities.Add(activity);
                }
                //return RedirectToAction("DelearEnquiry", "Admin");
                //return View();
                return PartialView("Details", dealearActivity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult ViewDetails(long id)
        {
            try
            {
                var existingEnquiry = _skodaContext.Delear_Enquiry.Where(x => x.Dealer_ID == id).Select(c => c).FirstOrDefault();

                var stages = _skodaContext.Enquiry_Status.Where(x => x.Enquiry_Id == existingEnquiry.Enquiry_id).Select(c => new { c.Stage_id, c.Enquiry_Id, c.Created_Date, c.ModifyDate }).FirstOrDefault();
                var Name = _skodaContext.Stages.Where(x => x.Id == stages.Stage_id).Select(c => new { c.Description }).FirstOrDefault();
                ViewBag.attachDocuments = _skodaContext.UploadDocument.Where(x => x.Delear_ID == existingEnquiry.Dealer_ID).ToList();
                ViewBag.StageName = Name.Description;
                ViewBag.StageId = stages.Stage_id;
                ViewBag.Enquiry_id = existingEnquiry.Enquiry_id;
                ViewBag.fileuploads = _skodaContext.Admin_FileUpload.Select(x => new { Value = x.Id, Text = x.FileName }).ToList();
                var Statusenq = "";
                int StatusId = existingEnquiry.Status_ID;
                if (StatusId == 1)
                {
                    Statusenq = "Open";
                }
                else if (StatusId == 2)
                {
                    Statusenq = "OnHold";
                }
                else if (StatusId == 3)
                {
                    Statusenq = "Rejected";
                }
                else if (StatusId == 4)
                {
                    Statusenq = "Approved";
                }
                ActivityViewDetails dealearActivity = new ActivityViewDetails();
                dealearActivity.Fullname = existingEnquiry.Fullname;
                dealearActivity.Email = existingEnquiry.Email;
                dealearActivity.Mobile = existingEnquiry.Mobile_No;
                int cityId = existingEnquiry.City_Id;
                dealearActivity.City = Convert.ToString(_skodaContext.City.Where(x => x.Id == cityId).Select(x => x.Name).FirstOrDefault());
                dealearActivity.Pincode = existingEnquiry.Pincode;
                dealearActivity.Bussiness_Type = existingEnquiry.Bussiness_Type;
                dealearActivity.Register_bussinessName = existingEnquiry.Register_bussinessName;
                dealearActivity.Brand_name = existingEnquiry.Brand_name;
                dealearActivity.About_businessExp = existingEnquiry.About_businessExp;
                dealearActivity.source = existingEnquiry.Source;
                dealearActivity.how_know = existingEnquiry.How_Know;
                if (existingEnquiry.BackgroundId == 1)
                    dealearActivity.Background = "Automobile 4 Wheeler";
                else if (existingEnquiry.BackgroundId == 2)
                    dealearActivity.Background = "Automobile Commercial vehicle & Tractors";
                else if (existingEnquiry.BackgroundId == 3)
                    dealearActivity.Background = "Automobile 3 or 2 Wheeler";
                else if (existingEnquiry.BackgroundId == 4)
                    dealearActivity.Background = "Automobile ancillary";
                else if (existingEnquiry.BackgroundId == 5)
                    dealearActivity.Background = "Other Auto related business";
                dealearActivity.OtherLocation = existingEnquiry.OtherLocation;
                dealearActivity.DocumentSubmitted = stages.Created_Date;
                dealearActivity.Modifydate = stages.ModifyDate ?? DateTime.Now;
                dealearActivity.Status = Statusenq;
                dealearActivity.Delear_Id = existingEnquiry.Dealer_ID;
                dealearActivity.activities = new List<Activity>();
                var dealerActivities = _skodaContext.DealerActivity.Where(x => x.Delear_ID == dealearActivity.Delear_Id).Take(5).OrderByDescending(x => x.Created_date).ToList();
                foreach (var a in dealerActivities)
                {
                    Activity activity = new Activity();
                    activity.CreatedDate = a.Created_date.ToString("hh:mm tt") + " | " + a.Created_date.ToString("dd/MM/yyyy");
                    activity.Description = a.Description;
                    dealearActivity.activities.Add(activity);
                }
                return View(dealearActivity);


            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        public IActionResult LoadDataDelear_enquiry()
        {
            try
            {
                UserDetails uInfo = null;
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                List<UserDelearEnquiry> userDelearEnquiries = new List<UserDelearEnquiry>();

                if (uInfo.Role == "Admin")
                {



                    userDelearEnquiries = (from e in _skodaContext.Delear_Enquiry
                                           join enq_staus in _skodaContext.Enquiry_Status on e.Dealer_ID
                                            equals enq_staus.Delear_Id
                                           join stage in _skodaContext.Stages
                                            on enq_staus.Stage_id equals stage.Id
                                           orderby e.Created_Date descending

                                           select new UserDelearEnquiry
                                           {
                                               Fullname = e.Fullname,
                                               Enquiry_id = e.Enquiry_id,
                                               Status_ID = e.Status_ID,
                                               NextStage = stage.Description,
                                               Bussiness_Type = e.Bussiness_Type,
                                               Dealer_ID = e.Dealer_ID,
                                               Owner = _skodaContext.Users.Where(x => x.UserId == enq_staus.User_Id).Select(x => x.Fullname).FirstOrDefault(),
                                               Created_Date = e.Created_Date.ToString("dd/MM/yyyy"),
                                               City = _skodaContext.City.Where(x => x.Id == e.City_Id).Select(x => x.Name).FirstOrDefault(),
                                               ZoneId = _skodaContext.City.Where(x => x.Id == e.City_Id).Select(x => x.ZoneId).FirstOrDefault(),
                                               //Open_Since = (DateTime.Now - e.Created_Date).Days.ToString() + " Days",9                                             
                                               Stage_Id = enq_staus.Stage_id,
                                               Open_Since = Math.Abs(_skodaContext.DealerActivity.Where(x => x.Delear_ID == e.Dealer_ID && x.Created_date != null).DefaultIfEmpty().Max(x => x.Created_date).Subtract(DateTime.Now.AddDays(1)).Days).ToString() + " Days",
                                               Source = e.Source
                                           }).ToList();
                }
                else
                {
                    var citylist = _skodaContext.City.Where(x => x.ZoneId == uInfo.ZoneId).Select(x => x.Id).ToList();
                    userDelearEnquiries = (from e in _skodaContext.Delear_Enquiry
                                           join enq_staus in _skodaContext.Enquiry_Status on e.Dealer_ID
                                           equals enq_staus.Delear_Id
                                           join stage in _skodaContext.Stages
                                           on enq_staus.Stage_id equals stage.Id
                                           // where e.City_Id == uInfo.CityId
                                           where citylist.Any(c => c == e.City_Id)
                                           orderby e.Created_Date descending
                                           select new UserDelearEnquiry
                                           {
                                               Fullname = e.Fullname,
                                               Enquiry_id = e.Enquiry_id,
                                               Status_ID = e.Status_ID,
                                               NextStage = stage.Description,
                                               Dealer_ID = e.Dealer_ID,
                                               Owner = _skodaContext.Users.Where(x => x.UserId == enq_staus.User_Id).Select(x => x.Fullname).FirstOrDefault(),
                                               Created_Date = e.Created_Date.ToString("dd/MM/yyyy"),
                                               City = _skodaContext.City.Where(x => x.Id == e.City_Id).Select(x => x.Name).FirstOrDefault(),
                                               ZoneId = _skodaContext.City.Where(x => x.Id == e.City_Id).Select(x => x.ZoneId).FirstOrDefault(),
                                               Stage_Id = enq_staus.Stage_id,
                                               //Open_Since = (DateTime.Now - e.Created_Date).Days.ToString() + " Days",
                                               // Open_Since = (_skodaContext.DealerActivity.Where(x => x.Delear_ID == e.Dealer_ID && x.Created_date != null).Max(x => x.Created_date) - e.Created_Date).Days.ToString() + " Days",
                                               Open_Since = Math.Abs(_skodaContext.DealerActivity.Where(x => x.Delear_ID == e.Dealer_ID && x.Created_date != null).DefaultIfEmpty().Max(x => x.Created_date).Subtract(e.Created_Date).Days).ToString() + " Days",
                                               Source = e.Source
                                           }).ToList();
                }
                foreach (var filename in userDelearEnquiries)
                {
                    ViewBag.FileName = _skodaContext.UploadDocument.Where(x => x.Delear_ID == filename.Dealer_ID).ToList();
                }

                return Json(JsonConvert.SerializeObject(userDelearEnquiries));

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IActionResult ActivityList(int delearId)
        {
            List<ActivityDetails> activities = new List<ActivityDetails>();
            UserDetails uInfo = null;
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["Skoda.DIAS.Admin"]))
            {
                return RedirectToAction("Login", "Admin");
            }
            else
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userBytes))
                {
                    uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
                }
                ViewBag.UserName = uInfo.Username;
                ViewBag.Role = uInfo.Role;
            }
            activities = _skodaContext.DealerActivity.Where(x => x.Delear_ID == delearId).OrderByDescending(x => x.Created_date).Select(e => new ActivityDetails
            {
                Description = e.Description,
                Delear_Id = e.Delear_ID,
                Comment = e.Comment,
                Created_date = e.Created_date.ToString("hh:mm tt") + " | " + e.Created_date.ToString("dd/MM/yyyy"),
                Id = e.Id
            }).ToList();

            return View(activities);
        }
        private List<ExportDetails> GetDetails()
        {
            List<ExportDetails> details = new List<ExportDetails>();
            var Enquiry = _skodaContext.Delear_Enquiry.Select(c => c).ToList();
            foreach (var enq in Enquiry)
            {
                ExportDetails ex = new ExportDetails();
                var stages = _skodaContext.Enquiry_Status.Where(x => x.Enquiry_Id == enq.Enquiry_id).Select(c => new { c.Stage_id, c.Enquiry_Id, c.Created_Date, c.ModifyDate }).FirstOrDefault();
                var Name = _skodaContext.Stages.Where(x => x.Id == stages.Stage_id).Select(c => new { c.Description }).FirstOrDefault();
                ex.StageName = Name.Description;
                ex.Created_Date = enq.Created_Date.ToString("dd/MM/yyyy");
                ex.Enquiry_id = enq.Enquiry_id;
                var Statusenq = "";
                if (enq.Status_ID == 1)
                {
                    Statusenq = "Open";
                }
                else if (enq.Status_ID == 2)
                {
                    Statusenq = "OnHold";
                }
                else if (enq.Status_ID == 3)
                {
                    Statusenq = "Rejected";
                }
                else if (enq.Status_ID == 4)
                {
                    Statusenq = "Approved";
                }

                ex.Fullname = enq.Fullname;
                ex.Email = enq.Email;
                ex.Mobile = enq.Mobile_No;
                int cityId = enq.City_Id;
                ex.City = Convert.ToString(_skodaContext.City.Where(x => x.Id == cityId).Select(x => x.Name).FirstOrDefault());
                ex.Pincode = enq.Pincode;
                ex.Bussiness_Type = enq.Bussiness_Type;
                ex.Register_bussinessName = enq.Register_bussinessName;
                ex.Brand_name = enq.Brand_name;
                ex.About_businessExp = enq.About_businessExp;
                ex.delear_ID = enq.Dealer_ID;
                ex.source = enq.Source;
                ex.how_know = enq.How_Know;
                DateTime dt = _skodaContext.DealerActivity.Where(x => x.Delear_ID == ex.delear_ID).OrderByDescending(x => x.Id).Select(x => x.Created_date).FirstOrDefault();
                ex.Open_Since = (dt - enq.Created_Date).Days.ToString() + " Days";
                ex.Salaried = enq.isSalaried == true ? "Yes" : "No";
                if (enq.BackgroundId == 1)
                    ex.Background = "Automobile 4 Wheeler";
                else if (enq.BackgroundId == 2)
                    ex.Background = "Automobile Commercial vehicle & Tractors";
                else if (enq.BackgroundId == 3)
                    ex.Background = "Automobile 3 or 2 Wheeler";
                else if (enq.BackgroundId == 4)
                    ex.Background = "Automobile ancillary";
                else if (enq.BackgroundId == 5)
                    ex.Background = "Other Auto related business";
                ex.OtherLocation = enq.OtherLocation;
                ex.DocumentSubmitted = stages.Created_Date;
                ex.Modifydate = stages.ModifyDate ?? DateTime.Now;
                ex.Status = Statusenq;
                ex.delear_ID = enq.Dealer_ID;
                ex.activities = new List<Activity>();
                ex.activities.AddRange((from e in _skodaContext.DealerActivity
                                        join enq1 in _skodaContext.Delear_Enquiry on e.Delear_ID
                                        equals enq1.Dealer_ID
                                        where e.Description != null
                                        select new Activity
                                        {
                                            Description = e.Description + " | " + e.Comment,
                                            Delear = enq1.Fullname,
                                            EnquiryId = enq.Enquiry_id,
                                            CreatedDate = e.Created_date.ToString("hh:mm tt") + " | " + e.Created_date.ToString("dd/MM/yyyy")
                                        }).ToList());

                details.Add(ex);
            }
            return details;
        }
        public IActionResult ExportEnquiry()
        {
            var Enquiry = this.GetDetails();
            byte[] fileContents;

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Delear_Enquiry");
            Sheet.Cells["A1"].Value = "Investor Name";
            Sheet.Cells["B1"].Value = "Enquiry ID";
            Sheet.Cells["C1"].Value = "Status";
            Sheet.Cells["D1"].Value = "Stage";
            Sheet.Cells["E1"].Value = "Created On";
            Sheet.Cells["F1"].Value = "Interested City";
            Sheet.Cells["G1"].Value = "Open Since";
            Sheet.Cells["H1"].Value = "Email";
            Sheet.Cells["I1"].Value = "Mobile";
            Sheet.Cells["J1"].Value = "Pincode";
            Sheet.Cells["K1"].Value = "Business Type";
            Sheet.Cells["L1"].Value = "Register business Name";
            Sheet.Cells["M1"].Value = "Source";
            Sheet.Cells["N1"].Value = "how_know";

            int row = 2;


            ExcelWorksheet Sheet1 = Ep.Workbook.Worksheets.Add("Activity");
            Sheet1.Cells["A1"].Value = "Activity";
            Sheet1.Cells["B1"].Value = "Date";
            Sheet1.Cells["C1"].Value = "Dealer";
            Sheet1.Cells["D1"].Value = "Enquiry_ID";



            foreach (var item in Enquiry)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.Fullname;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.Enquiry_id;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.Status;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.StageName;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.Created_Date;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.City;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.Open_Since;
                Sheet.Cells[string.Format("H{0}", row)].Value = item.Email;
                Sheet.Cells[string.Format("I{0}", row)].Value = item.Mobile;
                Sheet.Cells[string.Format("J{0}", row)].Value = item.Pincode;
                Sheet.Cells[string.Format("K{0}", row)].Value = item.Bussiness_Type;
                Sheet.Cells[string.Format("L{0}", row)].Value = item.Register_bussinessName;
                Sheet.Cells[string.Format("M{0}", row)].Value = item.source;
                Sheet.Cells[string.Format("N{0}", row)].Value = item.how_know;
                row++;
                int row1 = 2;
                foreach (var act in item.activities)
                {
                    if (act.Description != null)
                    {
                        Sheet1.Cells[string.Format("A{0}", row1)].Value = act.Description;
                        Sheet1.Cells[string.Format("B{0}", row1)].Value = act.CreatedDate;
                        Sheet1.Cells[string.Format("C{0}", row1)].Value = act.Delear;
                        Sheet1.Cells[string.Format("D{0}", row1)].Value = act.EnquiryId;
                        row1++;
                    }
                }
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();

            Sheet1.Cells["A:AZ"].AutoFitColumns();

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            fileContents = Ep.GetAsByteArray();

            if (fileContents == null || fileContents.Length == 0)
            {
                return NotFound();
            }

            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "Enquiry.xlsx"
            );
        }

    }
}