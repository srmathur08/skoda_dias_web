﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Skoda.DIAS.Common;
using Skoda.DIAS.Common.Helpers;
using Skoda.DIAS.Data.Models;
using Skoda.DIAS.Models;
using Skoda.DIAS.Services;
using Skoda.DIAS.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Skoda.DIAS.Common.Enums;

namespace Skoda.DIAS.UI.Controllers
{
    [Authorize]
    [SessionExpireAttribute]
    public class DelearController : BaseController
    {
        private readonly string DefaultLoginCookie = "Skoda.DIAS.AuthCookieAspNetCore";
        public readonly ILogger<DelearController> _logger;
        public SMTPSettings _smtpSettings;

        public DelearController(IWebHostEnvironment hostingEnvironment, ILogger<DelearController> logger, IConfiguration config, SkodaContext skodaContext, IOptions<SMTPSettings> smtpSettings)
            : base(hostingEnvironment, config, skodaContext)
        {
            _logger = logger;
            _smtpSettings = smtpSettings.Value;
        }
        public bool IsAuthenticated
        {
            get
            {
                if (HttpContext.Session.TryGetValue("DelearDetails", out _))
                {
                    return HttpContext.User.Identity.IsAuthenticated;
                }
                else
                    return false;
            }
        }
        [AllowAnonymous]
        public IActionResult Login(string ReturnUrl)
        {
            if (IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(ReturnUrl) && Url.IsLocalUrl(ReturnUrl))
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Login", "Delear");
                }
            }

            DelearLoginModel cm = new DelearLoginModel
            {
                ReturnUrl = ReturnUrl
            };

            if (HttpContext.Request.Cookies[DefaultLoginCookie] != null)
            {
                var cookieValue = HttpContext.Request.Cookies.Where(x => x.Key.Equals(DefaultLoginCookie)).FirstOrDefault().Value;
                SignupDetailModel userDetail = JsonConvert.DeserializeObject<SignupDetailModel>(EncryptDecryptUtils.Decrypt(PhraseKey, cookieValue));
                cm.MobileNo = userDetail.MobileNo;
            }

            return View(cm);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(DelearLoginModel loginModel)
        {
            try
            {
                if (await LoginUser(loginModel))
                {
                    if (!string.IsNullOrWhiteSpace(loginModel.ReturnUrl) && Url.IsLocalUrl(loginModel.ReturnUrl))
                    {
                        return Redirect(loginModel.ReturnUrl);
                    }

                    return RedirectToAction("Index", "Admin");
                }
            }
            catch (Exception ex)
            {
                loginModel.ErrorMessage = ex.Message;
            }

            return View(loginModel);
        }
        [AllowAnonymous]
        private async Task<bool> LoginUser(DelearLoginModel model)
        {
            bool completed = false;
            string strEmail = string.Empty, strBody, strSubject;
            var Dealear_id = _skodaContext.Delear_Enquiry.Where(x => x.Mobile_No == model.MobileNo).ToList().FirstOrDefault();
            string OTP = Convert.ToString(new Random().Next(100000, 999999));
            string Message = "Use OTP " + OTP + " to verify your mobile on SKODA-DIAS.";
            var delear = _skodaContext.Email_Template.Where(x => x.Template_ID == "OTP_Delear").FirstOrDefault();
            strSubject = delear.Subject;
            strBody = delear.Body.Replace("{user}", Dealear_id.Fullname)
                .Replace("{otp}", OTP).Replace("#link", AppUrl + "Delear/login");
            EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, Dealear_id.Email, strBody, strSubject, true, null);

            Verification verification = new Verification();
            verification.Code = OTP.ToString();
            verification.Delear_ID = Dealear_id.Dealer_ID;
            verification.SendCount = verification.Id == 0 ? 1 : verification.SendCount + 1;
            _skodaContext.Verification.Add(verification);
            _skodaContext.SaveChanges();
            //EmailServices.SendSMS(model.MobileNo, Message);
            return completed;
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<bool> LoginUsers(DelearLoginModel model)
        {
            try
            {
                return sendOTP(model.MobileNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<bool> SignupOTP(string mobile, string name, string email, int cityId)
        {
            try
            {
                return sendOTPSignup(mobile, name, email, cityId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private bool sendOTP(string mobile)
        {
            bool blnsuc = true;
            string strEmail = string.Empty, strBody, strSubject;
            var Dealear_id = _skodaContext.Delear_Enquiry.Where(x => x.Mobile_No == mobile).FirstOrDefault();
            if (Dealear_id != null)
            {
                if (Dealear_id.Dealer_ID > 0)
                {
                    string OTP = Convert.ToString(new Random().Next(100000, 999999));
                    string Message = "Use OTP " + OTP + " to verify your mobile on SKODA-DIAS.";
                    var delear = _skodaContext.Email_Template.Where(x => x.Template_ID == "OTP_Delear").FirstOrDefault();
                    strSubject = delear.Subject;
                    strBody = delear.Body.Replace("{user}", Dealear_id.Fullname)
                        .Replace("{otp}", OTP).Replace("#link", AppUrl + "Delear/login");
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, Dealear_id.Email, strBody, strSubject, true, null);

                    Verification verification = new Verification();
                    verification.Code = OTP.ToString();
                    verification.Delear_ID = Dealear_id.Dealer_ID;
                    verification.SendCount = verification.Id == 0 ? 1 : verification.SendCount + 1;
                    _skodaContext.Verification.Add(verification);
                    _skodaContext.SaveChanges();
                    blnsuc = false;
                }
            }
            return blnsuc;
        }
        private bool sendOTPSignup(string mobile, string name, string email, int cityId)
        {
            bool blnsuc = false;
            var Dealear_id = _skodaContext.Delear_Enquiry.Where(x => x.Mobile_No == mobile && x.City_Id == cityId).FirstOrDefault();
            if (Dealear_id == null)
            {
                string strEmail = string.Empty, strBody, strSubject;
                string OTP = Convert.ToString(new Random().Next(100000, 999999));
                string Message = "Use OTP " + OTP + " to verify your mobile on SKODA-DIAS.";
                var delear = _skodaContext.Email_Template.Where(x => x.Template_ID == "OTP_Delear").FirstOrDefault();
                strSubject = delear.Subject;
                strBody = delear.Body.Replace("{user}", name)
                    .Replace("{otp}", OTP).Replace("#link", AppUrl + "Delear/login");
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, email, strBody, strSubject, true, null);

                Verification verification = new Verification();
                verification.Code = OTP.ToString();
                verification.Delear_ID = 0;
                verification.SendCount = verification.Id == 0 ? 1 : verification.SendCount + 1;
                _skodaContext.Verification.Add(verification);
                _skodaContext.SaveChanges();
            }
            else
                blnsuc = true;
            return blnsuc;
        }
        public IActionResult Profile()
        {
            UserDetails uInfo = null;

            if (HttpContext.Session.TryGetValue("DelearLoginWithOtp", out byte[] userBytes))
            {
                uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }

            var profile = (from e in _skodaContext.Users
                           where e.IsDeleted == false
                           select new UserProfile
                           {
                               FullName = e.Fullname,
                               Username = e.Username,
                               UserId = e.UserId,
                               RoleId = e.RoleId
                           }).Where(x => x.UserId == uInfo.UserId).FirstOrDefault();
            ViewBag.UserName = uInfo.Username;


            var roles = (from role in _skodaContext.Roles.Where(r => !r.IsDeleted)
                         select new SelectListItem()
                         {
                             Text = role.Name,
                             Value = role.RoleId.ToString(),
                             Selected = profile.RoleId > 0 ? profile.RoleId == role.RoleId : false
                         }).ToList();

            roles.Insert(0, new SelectListItem()
            {
                Text = "----Select----",
                Value = string.Empty,
            });
            profile.Roles = roles;
            EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, uInfo.Email, "Hello " + uInfo.Fullname + ",<br/><br/>" + "Your Profile has been updated sucessfully. <br/><br/>Thanks,<br/>Skoda DIAS Team", "Your Profile has been updated", true, null);
            return View(profile);
        }

        [HttpPost]
        public IActionResult Profile(UserProfile model)
        {
            UserDetails uInfo = null;

            if (HttpContext.Session.TryGetValue("DelearLoginWithOtp", out byte[] userBytes))
            {
                uInfo = ObjectToByte.ByteArrayToObject<UserDetails>(userBytes);
            }


            var roles = (from role in _skodaContext.Roles.Where(r => !r.IsDeleted)
                         select new SelectListItem()
                         {
                             Text = role.Name,
                             Value = role.RoleId.ToString(),
                             Selected = model.RoleId > 0 ? model.RoleId == role.RoleId : false
                         }).ToList();

            roles.Insert(0, new SelectListItem()
            {
                Text = "----Select----",
                Value = string.Empty,
            });
            model.Roles = roles;

            var emp = _skodaContext.Users.Where(x => x.UserId == uInfo.UserId).FirstOrDefault();
            emp.Username = model.Username;
            emp.Fullname = model.FullName;
            emp.UserId = model.UserId;
            emp.RoleId = model.RoleId;
            _skodaContext.Update(emp);
            _skodaContext.SaveChanges();
            return RedirectToAction("List", "Dashboard");
        }
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            HttpContext.Session.Clear();

            return RedirectToAction("Login");
        }
        [AllowAnonymous]
        public IActionResult Signup()
        {
            SignupModel model = new SignupModel();

            var state = (from c in _skodaContext.State
                         orderby c.Name ascending
                         select new SelectListItem()
                         {
                             Text = c.Name,
                             Value = c.Id.ToString(),
                             Selected = model.CityId > 0 ? model.CityId == c.Id : false
                         }).ToList();
            state.Insert(0, new SelectListItem()
            {
                Text = "State",
                Value = string.Empty,
            });


            model.States = state;

            var ocity = (from c1 in _skodaContext.City.Where(x => x.IsOpen == false)
                         orderby c1.Name ascending
                         select new SelectListItem()
                         {
                             Text = c1.Name,
                             Value = c1.Id.ToString(),
                             Selected = model.OCityId > 0 ? model.OCityId == c1.Id : false
                         }).ToList();
            model.OCities = ocity;


            return View(model);

        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Signup(SignupModel model, string otp)
        {
            string msg = string.Empty;
            int citiId = 0;
            // var getExistingOtp = _skodaContext.Verification.Where(x => x.Delear_ID == singlerecord.Dealer_ID).ToList().LastOrDefault();
            //temp code for testing purpose
            if (otp == "123456" && ModelState.IsValid)
            {
                if (model.CityId == 0)
                    citiId = model.OCityId;
                else
                    citiId = model.CityId;
                model.City = _skodaContext.City.Where(x => x.Id == citiId).Select(x => x.Name).FirstOrDefault();
            }
            HttpContext.Session.Set("DelearDetails", ObjectToByte.ObjectToByteArray(model));
            msg = "success";
            return Json(msg);

        }
        [AllowAnonymous]
        public IActionResult LoginOTP()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult LoginOTP(string otp, string mobilenumber)
        {
            int delearid = _skodaContext.Delear_Enquiry.Where(x => x.Mobile_No == mobilenumber).Select(m => m.Dealer_ID).FirstOrDefault();
            var msg = "";
            if (delearid > 0)
            {
                var singlerecord = _skodaContext.Delear_Enquiry.Where(x => x.Dealer_ID == delearid).Select(c => new { c.Dealer_ID, c.Mobile_No, c.Email, c.Fullname, c.Enquiry_id }).FirstOrDefault();

                if (singlerecord != null)
                {
                    var getExistingOtp = _skodaContext.Verification.Where(x => x.Delear_ID == singlerecord.Dealer_ID).ToList().LastOrDefault();
                    //temp code for testing purpose
                    if (otp == "123456" || otp == getExistingOtp.Code)
                    {
                        Enquiry m = new Enquiry();
                        m.Mobile = singlerecord.Mobile_No;
                        HttpContext.Session.Set("DelearLogin", ObjectToByte.ObjectToByteArray(m));
                        int count = _skodaContext.Delear_Enquiry.Where(x => x.Mobile_No == mobilenumber).Count();
                        if (count == 1)
                            msg = singlerecord.Enquiry_id;
                        else
                            msg = "multi";
                        return Json(msg);
                    }
                    if (otp != null)
                        EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, singlerecord.Email, "Hello " + singlerecord.Fullname + ",<br/><br/>" + "Your OTP Is :" + otp + " <br/><br/>Thanks,<br/>Skoda DIAS Team", "Login OTP", true, null);
                }

            }
            else if (delearid <= 0)
            {
                msg = "alert";
                //return Json(msg);
            }
            return Json(msg);
        }
        [AllowAnonymous]
        public IActionResult SignupDetail()
        {
            SignupModel dInfo = null;
            SignupDetailModel model = new SignupDetailModel();

            if (HttpContext.Session.TryGetValue("DelearDetails", out byte[] delearBytes))
            {
                dInfo = ObjectToByte.ByteArrayToObject<SignupModel>(delearBytes);
            }

            model.Background = new List<Itemlist>()
                {
                    new Itemlist { Text = "Automobile 4 Wheeler", Value = 1 },
                    new Itemlist { Text = "Automobile Commercial vehicle & Tractors", Value = 2 },
                    new Itemlist { Text = "Automobile 3 or 2 Wheeler", Value = 3},
                    new Itemlist { Text = "Automobile ancillary", Value = 4 },
                    new Itemlist { Text = "Multiple Automobile business", Value = 5 },
                    new Itemlist { Text = "Other Auto related business", Value = 6 }
                };

            DashboardModel dashboardModel = new DashboardModel();
            if (dInfo != null)
            {
                ViewBag.FullName = dInfo.Fullname;
                ViewBag.MobileNo = dInfo.MobileNo;
                ViewBag.City = dInfo.City;
                ViewBag.Email = dInfo.Email;
                ViewBag.Citys = _skodaContext.City.Where(x => x.Name == dInfo.City).Select(c => c.IsOpen).FirstOrDefault();
            }

            return View(model);

        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult GetPincode(int id)
        {
            string pincodeval = _skodaContext.City.Where(x => x.Id == id).Select(c => c.Pincode).FirstOrDefault();
            if (pincodeval != null)
            {
                string[] pincode = pincodeval.Split(",");
                return Json(pincode[0]);
            }
            else
                return Json(0);
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult GetCity(string pincode)
        {
            var cityid = _skodaContext.City.Where(x => x.Pincode.Contains(pincode)).Select(c => new { c.Id, c.IsOpen }).FirstOrDefault();
            return Json(cityid);
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult GetOtherCity(string pincode)
        {
            var cityid = _skodaContext.City.Where(x => x.Pincode.Contains(pincode)).Select(c => new { c.Id, c.IsOpen }).FirstOrDefault();
            return Json(cityid);
        }
        [AllowAnonymous]
        [HttpGet]
        public JsonResult FillState(int Id)
        {
            List<Alias> AreaMgr = new List<Alias>();
            var state = (from c in _skodaContext.State.Where(x => x.ZoneId == Id)
                        orderby c.Name ascending
                        select new Alias()
                        {
                            Id = c.Id,
                            Name = c.Name
                        }).ToList(); 
            return Json(state);
        }
        [AllowAnonymous]
        [HttpGet]
        public JsonResult FillCity(int Id)
        {
            List<Alias> AreaMgr = new List<Alias>();
            var city = (from c in _skodaContext.City.Where(x => x.IsOpen == true && x.StateId == Id)
                        orderby c.Name ascending
                        select new Alias()
                        {
                            Id = c.Id,
                            Name = c.Name
                        }).ToList();
            city.Add(new Alias() { Name = "Other", Id = 0 });
            return Json(city);
        }
        [AllowAnonymous]
        [HttpGet]
        public JsonResult FillOtherCity(int StateId)
        {
            List<Alias> AreaMgr = new List<Alias>();
            var city = (from c in _skodaContext.City.Where(x => x.IsOpen == false && x.StateId == StateId)
                        orderby c.Name ascending
                        select new Alias()
                        {
                            Id = c.Id,
                            Name = c.Name
                        }).ToList();
           // city.Add(new Alias() { Name = "Other", Id = 0 });
            return Json(city);

        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult SignupDetail(SignupDetailModel Detailmodel)
        {
            SignupModel dInfo = null;
            if (HttpContext.Session.TryGetValue("DelearDetails", out byte[] delearByte))
            {
                dInfo = ObjectToByte.ByteArrayToObject<SignupModel>(delearByte);
            }
            ModelState.Remove("BackgroundId");
            if (_skodaContext.City.Where(x => x.Name == dInfo.City).Select(c => c.IsOpen).FirstOrDefault() == false)
            {
                ModelState.Remove("About_BussinessExp");
                ModelState.Remove("Source");
            }
            if (Detailmodel.issalaried == true)
            {
                ModelState.Remove("Register_BussinessName");
                ModelState.Remove("Brand_name");

            }
            if (!ModelState.IsValid)
            {

                if (dInfo != null)
                {
                    ViewBag.FullName = dInfo.Fullname;
                    ViewBag.MobileNo = dInfo.MobileNo;
                    ViewBag.City = dInfo.City;
                    ViewBag.Email = dInfo.Email;

                }
                Detailmodel.Background = new List<Itemlist>()
                {
                    new Itemlist { Text = "Automobile 4 Wheeler", Value = 1 },
                    new Itemlist { Text = "Automobile Commercial vehicle & Tractors", Value = 2 },
                    new Itemlist { Text = "Automobile 3 or 2 Wheeler", Value = 3},
                    new Itemlist { Text = "Automobile ancillary", Value = 4 },
                    new Itemlist { Text = "Other Auto related business", Value = 5 }
                };
                return View(Detailmodel);
            }

            var tempFileName = _skodaContext.Delear_Enquiry
                                 .OrderByDescending(x => x.Dealer_ID)
                                 .Take(1)
                                 .Select(x => x.Enquiry_id)
                                 .ToList()
                                 .FirstOrDefault();
            int tmp = Convert.ToInt32(tempFileName.Split('Q')[1]);
            tmp = tmp + 1;
            int cityId = 0;
            string otherloc = string.Empty;
            Boolean IsSelectOther = _skodaContext.City.Where(x => x.Name == dInfo.City).Select(c => c.IsOpen).FirstOrDefault();
            int Status;
            if (IsSelectOther)
            {
                Status = (int)Common.Enums.Status.Open;
            }
            else
            {
                Status = (int)Common.Enums.Status.Rejected;
            }
            if (dInfo.CityId != 0)
                cityId = dInfo.CityId;
            else
            {
                cityId = dInfo.OCityId;
                otherloc = _skodaContext.City.Where(x => x.Id == dInfo.OCityId).Select(x => x.Name).FirstOrDefault();
            }
            if (Detailmodel.Bussiness_Type != "Bussiness(Automobile)")
                Detailmodel.BackgroundId = 0;
            var deleardetails = new Delear_Enquiry()
            {
                Fullname = dInfo.Fullname,
                Mobile_No = dInfo.MobileNo,
                Email = dInfo.Email,
                City_Id = cityId,
                Pincode = dInfo.Pincode,
                Bussiness_Type = Detailmodel.Bussiness_Type,
                Register_bussinessName = Detailmodel.Register_BussinessName,
                About_businessExp = Detailmodel.About_BussinessExp,
                Source = Detailmodel.Source,
                Brand_name = Detailmodel.Brand_name,
                Created_Date = DateTime.Now,
                Enquiry_id = "ENQ0000" + tmp,
                isSalaried = false,
                Status_ID = Status,
                How_Know = Detailmodel.How_Know,
                BackgroundId = Detailmodel.BackgroundId,
                State_Id = dInfo.StateId,
                OtherLocation = otherloc
            };
            _skodaContext.Delear_Enquiry.Add(deleardetails);
            _skodaContext.SaveChanges();
            Enquiry_Status EnquiryStatus = new Enquiry_Status();
            EnquiryStatus.Enquiry_Id = "ENQ0000" + tmp;
            EnquiryStatus.Delear_Id = deleardetails.Dealer_ID;
            EnquiryStatus.User_Id = 0;
            EnquiryStatus.Stage_id = Convert.ToInt32(Stage.RequestDoc);
            EnquiryStatus.Status_id = Status;
            EnquiryStatus.Created_Date = DateTime.Now;
            EnquiryStatus.ModifyDate = null;
            _skodaContext.Enquiry_Status.Add(EnquiryStatus);


            Notifications notify = new Notifications();
            notify.CreatedDate = DateTime.Now;
            notify.IsDeleted = false;
            notify.IsRead = false;
            notify.Title = "Dealer Enquiry";
            notify.Message = "Dealer Enquiry Submitted By-" + dInfo.Fullname;
            notify.DelearId = deleardetails.Dealer_ID;
            notify.UserId = 0;
            _skodaContext.Notifications.Add(notify);
            string AdminEmail = _skodaContext.Users.Where(x => x.Username == "admin").Select(x => x.Email).FirstOrDefault();
            string strBody, strSubject, adminstrBody, adminstrSubject;
            DealerActivity activity = new DealerActivity();
            activity.Created_date = DateTime.Now;
            activity.Delear_ID = deleardetails.Dealer_ID;
            activity.Description = "Dealer enquiry submitted successfully.";
            activity.Comment = "Dealer Enquiry Submitted By-" + dInfo.Fullname;
            _skodaContext.DealerActivity.Add(activity);
            _skodaContext.SaveChanges();
            if (!IsSelectOther)
            {
                activity = new DealerActivity();
                activity.Created_date = DateTime.Now;
                activity.Delear_ID = deleardetails.Dealer_ID;
                activity.Description = "Dealer enquiry rejected successfully.";
                activity.Comment = "Dealer enquiry rejected by-" + dInfo.Fullname;
                _skodaContext.DealerActivity.Add(activity);
                _skodaContext.SaveChanges();
            }

            var CityName = _skodaContext.City.Where(x => x.Id == deleardetails.City_Id).Select(c => c.Name).FirstOrDefault();
            try
            {

                var emails = _skodaContext.Email_Template.Where(x => x.Template_ID == "signup_opencity").FirstOrDefault();
                strBody = emails.Body.Replace("{Dealer User Full Name }", deleardetails.Fullname).Replace("{Enq.ID}", EnquiryStatus.Enquiry_Id).Replace("{Dealer portal Login Link}", AppUrl + "Delear/login");
                strSubject = emails.Subject.Replace("#enqid#", EnquiryStatus.Enquiry_Id).Replace("#city#", CityName);
                string[] bcco = _smtpSettings.OpenCityAdd.Split(",");
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, deleardetails.Email, strBody, strSubject, true, null, bcco);
                if (deleardetails.OtherLocation != "")
                {
                    string[] bcc = _smtpSettings.BccAddress.Split(",");
                    var emailother = _skodaContext.Email_Template.Where(x => x.Template_ID == "signup_nonopencity").FirstOrDefault();
                    strBody = emailother.Body.Replace("{Dealer User Full Name }", deleardetails.Fullname).Replace("{Enq.ID}", EnquiryStatus.Enquiry_Id).Replace("{Dealer portal Login Link}", AppUrl + "Delear/login");
                    strSubject = emails.Subject.Replace("#enqid#", EnquiryStatus.Enquiry_Id).Replace("#city#", CityName);
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, deleardetails.Email, strBody, strSubject, true, null, bcc);
                   
                    var Adminother = _skodaContext.Email_Template.Where(x => x.Template_ID == "signup_Network_Team_rejected").FirstOrDefault();
                    strBody = Adminother.Body.Replace("#enqID#", EnquiryStatus.Enquiry_Id);
                    strSubject = Adminother.Subject.Replace("#city#", CityName);
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, AdminEmail, strBody, strSubject, true, null);
                }
                else
                {
                    var email = _skodaContext.Email_Template.Where(x => x.Template_ID == "signup_networkuser").FirstOrDefault();
                    adminstrBody = email.Body.Replace("#1", EnquiryStatus.Enquiry_Id).Replace("#2", deleardetails.Fullname).Replace("#3", deleardetails.Email)
                        .Replace("#4", deleardetails.Mobile_No).Replace("#5", CityName).Replace("#6", deleardetails.Bussiness_Type).Replace("#7", deleardetails.Register_bussinessName).Replace("#8", deleardetails.Brand_name)
                        .Replace("#9", deleardetails.About_businessExp).Replace("#user", deleardetails.Fullname)
                        .Replace("#linka", AppUrl + "Admin/Enquirystatus?s=1&id=" + deleardetails.Dealer_ID + "")
                        .Replace("#linkr", AppUrl + "Admin/Enquirystatus?s=2&id=" + deleardetails.Dealer_ID + "")
                        .Replace("#link", AppUrl + "Admin/login").Replace("#Source", deleardetails.Source).Replace("#How", deleardetails.How_Know);
                    adminstrSubject = email.Subject.Replace("#enqid#", EnquiryStatus.Enquiry_Id).Replace("#city#", CityName);

                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, AdminEmail, adminstrBody, adminstrSubject, true, null);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Sending Email to", _smtpSettings.FromAddress, Detailmodel.MobileNo);
            }
            return RedirectToAction("Thankyou");
        }

        [AllowAnonymous]
        public IActionResult Thankyou()
        {
            SignupModel model = null;
            if (HttpContext.Session.TryGetValue("DelearDetails", out byte[] delearBytes))
            {
                model = ObjectToByte.ByteArrayToObject<SignupModel>(delearBytes);
            }
            return View(model);
        }


    }
}