﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Skoda.DIAS.Common;
using Skoda.DIAS.Data.Models;
using Skoda.DIAS.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Skoda.DIAS.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class ChangePasswordController : BaseController
    {
       
        public ChangePasswordController(IWebHostEnvironment hostingEnvironment, ILogger<AdminController> logger, IConfiguration config, SkodaContext skodaContext, IOptions<SMTPSettings> smtpSettings)
            : base(hostingEnvironment, config, skodaContext)
        {
        }


        public IActionResult Index(int id)
        {
            var changePasswordModel = new ChangePasswordModel() {
                UserId = id,
                OldPassword = "",
                NewPassword = "",
                ConfirmPassword = ""
            };
            return View(changePasswordModel);
        }

        [HttpPost]
        public IActionResult Index(ChangePasswordModel model) {

            if (model != null && model.OldPassword != null && model.OldPassword != "" &&
                model.NewPassword != null && model.NewPassword != "" &&
                model.ConfirmPassword != null && model.ConfirmPassword != "")
            {
                var encPassword = EncryptDecryptUtils.Encrypt(PhraseKey, Convert.ToString(model.OldPassword));
                var exuser = _skodaContext.Users.Where(u => u.UserId == model.UserId && u.Password == encPassword).FirstOrDefault();
                if (exuser != null)
                {
                    if (model.NewPassword == model.ConfirmPassword)
                    {
                        var encNewPassword = EncryptDecryptUtils.Encrypt(PhraseKey, Convert.ToString(model.NewPassword));
                        exuser.Password = encNewPassword;
                        _skodaContext.Users.Update(exuser);
                        _skodaContext.SaveChanges();
                        return RedirectToAction("Profile", "Admin");
                    }
                    else {
                        ViewBag.Message = "New Password and Confirm Password didn't match";
                    }
                }
                else {
                    ViewBag.Message = "Please enter correct existing password";
                }
            }
            else {
                ViewBag.Message = "All fields are mandatory";
            }

            return View();
        }
    }
}
