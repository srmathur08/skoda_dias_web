﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Skoda.DIAS.Data.Models;

namespace Skoda.DIAS.UI.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IWebHostEnvironment _hostingEnvironment;
        protected readonly IConfiguration _config;
        protected readonly SkodaContext _skodaContext;
        protected readonly string PhraseKey;
        protected readonly string AppUrl;

        public BaseController(IWebHostEnvironment hostingEnvironment, IConfiguration config, SkodaContext skodaContext)
        {
            _hostingEnvironment = hostingEnvironment;
            _config = config;
            _skodaContext = skodaContext;
            PhraseKey = _config.GetValue<string>("PhraseKey");
            AppUrl = _config.GetValue<string>("AppUrl");
        }
    }
}
