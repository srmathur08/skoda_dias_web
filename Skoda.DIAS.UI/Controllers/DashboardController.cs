﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Skoda.DIAS.Common;
using Skoda.DIAS.Common.Helpers;
using Skoda.DIAS.Data.Models;
using Skoda.DIAS.Models;
using Skoda.DIAS.Services;
using Skoda.DIAS.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using System.IO;
using Skoda.DIAS.Common.Enums;
using System.Text;
using Microsoft.Extensions.Primitives;

namespace Skoda.DIAS.UI.Controllers
{
    [Authorize]
    [SessionExpireAttribute]
    public class DashboardController : BaseController
    {
        public readonly ILogger<DelearController> _logger;
        public SMTPSettings _smtpSettings;
        private IHostingEnvironment _environment;
        public DashboardController(IWebHostEnvironment hostingEnvironment, ILogger<DelearController> logger, IConfiguration config, SkodaContext skodaContext, IOptions<SMTPSettings> smtpSettings, IHostingEnvironment environment)
            : base(hostingEnvironment, config, skodaContext)
        {
            _logger = logger;
            _smtpSettings = smtpSettings.Value;
            _environment = environment;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> Download(string file)
        {
            string FilePath = Path.Combine(_environment.WebRootPath, "uploads") + "/" + GetEnquiryId() + "/" + file;
            var memory = new MemoryStream();
            using (var stream = new FileStream(FilePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(FilePath), Path.GetFileName(FilePath));
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> MasterDownload(string file)
        {
            string FilePath = Path.Combine(_environment.WebRootPath, "uploads") + "/documents/" + file;
            var memory = new MemoryStream();
            using (var stream = new FileStream(FilePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(FilePath), Path.GetFileName(FilePath));
        }
        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
        {".txt", "text/plain"},
        {".pdf", "application/pdf"},
        {".ppt", "application/x-mspowerpoint"},
        {".pptx", "application/x-mspowerpoint"},
        {".pps", "application/x-mspowerpoint"},
        {".doc", "application/vnd.ms-word"},
        {".docx", "application/vnd.ms-word"},
        {".xls", "application/vnd.ms-excel"},
        {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
        {".png", "image/png"},
        {".jpg", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".gif", "image/gif"},
        {".csv", "text/csv"}
            };
        }
        private string GetEnquiryId()
        {
            DealerModel dInfo = null;
            if (HttpContext.Session.TryGetValue("DelearLoginWithOtp", out byte[] delearBytes))
            {
                dInfo = ObjectToByte.ByteArrayToObject<DealerModel>(delearBytes);
            }
            return dInfo.Enquiry_id;
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult List()
        {
            Enquiry uInfo = null;

            if (HttpContext.Session.TryGetValue("DelearLogin", out byte[] user))
            {
                uInfo = ObjectToByte.ByteArrayToObject<Enquiry>(user);
            }
            List<DealerList> model = new List<DealerList>();
            model = (from c in _skodaContext.Delear_Enquiry
                     where c.Mobile_No == uInfo.Mobile
                     select new DealerList()
                     {
                         Brand = c.Brand_name,
                         BusinessName = c.Brand_name,
                         City = _skodaContext.City.Where(x => x.Id == c.City_Id).Select(x => x.Name).FirstOrDefault(),
                         EnquiryId = c.Enquiry_id,
                         BusinessType = c.Bussiness_Type,
                         FullName = c.Fullname,
                         Pincode = c.Pincode,
                         CreatedDt = c.Created_Date.ToString("dd MM yyyy")
                     }).ToList();
            return View(model);
        }
        [AllowAnonymous]
        public IActionResult Index(string id)
        {

            var singlerecord = _skodaContext.Delear_Enquiry.Where(x => x.Enquiry_id == id).Select(c => new { c.Status_ID, c.Enquiry_id, c.Created_Date, c.Mobile_No, c.Email, c.Bussiness_Type, c.City_Id, c.Fullname, c.Dealer_ID, c.State_Id }).FirstOrDefault();
            ViewBag.DealerName = singlerecord.Fullname;
            ViewBag.BussinessRegistered = singlerecord.Created_Date;

            DealerModel model = new DealerModel();
            model.Fullname = singlerecord.Fullname;
            model.Enquiry_id = singlerecord.Enquiry_id;
            model.Mobile_No = singlerecord.Mobile_No;
            model.Dealer_ID = singlerecord.Dealer_ID;
            model.Email = singlerecord.Email;
            model.City = _skodaContext.City.Where(x => x.Id == singlerecord.City_Id).Select(y => y.Name).FirstOrDefault();
            model.Bussiness_Type = singlerecord.Bussiness_Type;
            HttpContext.Session.Set("DelearLoginWithOtp", ObjectToByte.ObjectToByteArray(model));

            var StatusId = singlerecord.Status_ID;
            if (StatusId == 1)
            {
                ViewBag.Status = "Open";
            }
            else if (StatusId == 2)
            {
                ViewBag.Status = "OnHold";
            }
            else if (StatusId == 3)
            {
                ViewBag.Status = "Rejected";
            }
            else if (StatusId == 4)
            {
                ViewBag.Status = "Approved";
            }
            var stages = _skodaContext.Enquiry_Status.Where(x => x.Enquiry_Id == singlerecord.Enquiry_id).Select(c => new { c.Stage_id, c.Enquiry_Id, c.Created_Date, c.ModifyDate }).FirstOrDefault();
            if (stages != null)
            {
                ViewBag.DocumentSubmitted = stages.Created_Date;
                ViewBag.modifyDate = stages.ModifyDate;
                var msg = "";
                if (stages.Stage_id > 0)
                {
                    var Name = _skodaContext.Stages.Where(x => x.Id == stages.Stage_id).Select(c => new { c.Description }).FirstOrDefault();
                    ViewBag.StageName = Name.Description;

                    ViewBag.Stage = stages.Stage_id;
                }
                else
                {
                    msg = "Enquiry ID does not exist!";
                    return Json(msg);
                }
            }
            List<DealerActivity> activity = new List<DealerActivity>();
            var tmp = _skodaContext.DealerActivity.Where(x => x.Delear_ID == singlerecord.Dealer_ID).OrderByDescending(x => x.Created_date).ToList();
            foreach (var line in tmp)
            {
                if (line.Description != "")
                {
                    var Isadmin = line.Description.Split(" ")[0];
                    if (Isadmin != "Admin")
                    {
                        activity.Add(new DealerActivity { Id = line.Id, Description = line.Description, Delear_ID = line.Delear_ID, Comment = line.Comment, Created_date = line.Created_date });
                    }
                }
                //Console.WriteLine(line);
            }
            ViewBag.activities = activity;
            ViewBag.attachDocuments = _skodaContext.UploadDocument.Where(x => x.Delear_ID == singlerecord.Dealer_ID).OrderByDescending(x => x.Created_date).ToList();
            ViewBag.MasterDocuments = _skodaContext.Admin_FileUpload.OrderByDescending(x => x.CreatedDate).ToList();
            ViewBag.Count = _skodaContext.Delear_Enquiry.Where(x => x.Mobile_No == singlerecord.Mobile_No).Count();
            var Zoneid = _skodaContext.State.Where(x => x.Id == singlerecord.State_Id).FirstOrDefault();
            if (Zoneid != null)
            {
                var ZoneData = _skodaContext.Zone.Where(x => x.Id == Zoneid.ZoneId).FirstOrDefault();
                var ZoneDatas = _skodaContext.Support_Email.Where(x => x.Id == Zoneid.ZoneId).FirstOrDefault();
                Location student2 = new Location
                {
                    ZoneName = ZoneData.Name,
                    FullName = ZoneDatas.FullName,
                    Email = ZoneDatas.Email
                };
                ViewBag.Support = student2;
            }
            return View();
        }
        public class SomeQuery
        {
            public string Para { get; set; }
        }
        public class Location
        {
            public string ZoneName { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult FileUpload()
        {
            var filelist = HttpContext.Request.Form.Files;

            DealerModel dInfo = null;
            if (HttpContext.Session.TryGetValue("DelearLoginWithOtp", out byte[] delearBytes))
            {
                dInfo = ObjectToByte.ByteArrayToObject<DealerModel>(delearBytes);
            }
            if (dInfo != null)
            {
                string uploadRoot = Path.Combine(_environment.WebRootPath, "uploads");
                string folder = string.Format(uploadRoot + "/{0}/", dInfo.Enquiry_id);
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);   // to create folder

                }
                StringBuilder HStable = new StringBuilder();

                if (filelist.Count > 0)
                {
                    HStable.Append("<table class='skd-prf-tbl' cellspacing='0' style='border-collapse:collapse; width:100%'>");
                    HStable.Append("<tr><th style = 'background:#e9f1e8; text-align:center;width:50%'>File Name</th><th style ='background:#e9f1e8; text-align:center'> Download Links </th></tr>");
                    foreach (var file in filelist)
                    {
                        var uploads = Path.Combine(_environment.WebRootPath, folder);
                        string FileName = dInfo.Fullname + "_" + dInfo.Enquiry_id + "_" + file.FileName;
                        UploadDocument uploadDocument = new UploadDocument();
                        uploadDocument.Upload_Document = FileName;
                        uploadDocument.Created_date = DateTime.UtcNow;
                        uploadDocument.Delear_ID = dInfo.Dealer_ID;
                        _skodaContext.UploadDocument.Add(uploadDocument);
                        HStable.Append("<tr><td style='text-align:center; width:50%'>" + FileName + "</td>");
                        HStable.Append("<td style='text-align:center;'><a href='" + AppUrl + "/uploads/" + dInfo.Enquiry_id + "/" + FileName + "' >" + FileName + "</a></td></tr>");

                        using (var fileStream = new FileStream(Path.Combine(uploads, FileName), FileMode.Create))
                        {

                            file.CopyTo(fileStream);
                        }
                    }
                    HStable.Append("</table>");
                    string strBody, strSubject, adminstrBody, adminstrSubject;
                    var email = _skodaContext.Email_Template.Where(x => x.Template_ID == "uploaddocument_delear").FirstOrDefault();
                    strBody = email.Body.Replace("{table}", HStable.ToString()).Replace("{ Dealer User Full Name }", dInfo.Fullname)
                        .Replace("{Dealer Portal link}", AppUrl + "Delear/login");
                    strSubject = email.Subject.Replace("#city#", dInfo.City);
                    var adminemail = _skodaContext.Email_Template.Where(x => x.Template_ID == "Uploaddocument_Network_Team").FirstOrDefault();
                    adminstrBody = adminemail.Body.Replace("{table}", HStable.ToString()).Replace("{ Dealer User Fullname }", dInfo.Fullname)
                        .Replace("{Dealer Portal link}", AppUrl + "Admin/login");
                    adminstrSubject = adminemail.Subject.Replace("#city#", dInfo.City);
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, dInfo.Email, strBody, strSubject, true, null);
                    string AdminEmail = _skodaContext.Users.Where(x => x.Username == "admin").Select(x => x.Email).FirstOrDefault();
                    EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, AdminEmail, adminstrBody, adminstrSubject, true, null);

                    DealerActivity activity = new DealerActivity();
                    activity.Created_date = DateTime.Now;
                    activity.Delear_ID = dInfo.Dealer_ID;
                    activity.Description = dInfo.Fullname + " has submitted the Documents.";
                    activity.Comment = dInfo.Fullname + " has submitted the Documents.";
                    _skodaContext.DealerActivity.Add(activity);

                    Notifications notify = new Notifications();
                    notify.CreatedDate = DateTime.Now;
                    notify.IsDeleted = false;
                    notify.IsRead = false;
                    notify.Title = "File Uploaded";
                    notify.Message = "File Uploaded Successfully By-" + dInfo.Fullname;
                    notify.DelearId = dInfo.Dealer_ID;
                    notify.UserId = 0;
                    _skodaContext.Notifications.Add(notify);

                    _skodaContext.SaveChanges();

                    ViewBag.Path = folder;
                    ViewBag.fileUpload = filelist;
                }
            }
            if (_skodaContext.Delear_Enquiry.Where(x => x.Mobile_No == dInfo.Mobile_No).Count() > 1)
                return RedirectToAction("List");
            else
                return RedirectToAction("Index", new { id = dInfo.Enquiry_id });
        }
    }
}