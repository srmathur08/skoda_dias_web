﻿using Skoda.DIAS.Common.Helpers;
using Skoda.DIAS.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace Skoda.DIAS.UI.Helpers
{
    public class AdminAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ISession session = filterContext.HttpContext.Session;

            if (filterContext.Controller is Controller)
            {
                //var userDetails = (UserDetails)SessionData.GetUserDetails(filterContext.HttpContext);

                if (session != null)
                {
                    filterContext.Result =
                           new RedirectToRouteResult(
                               new RouteValueDictionary{{ "controller", "Admin" },
                                          { "action", "Login" }
                            });
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}