using Skoda.DIAS.Data.Models;
using Skoda.DIAS.UI.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Microsoft.AspNetCore.DataProtection;
using System.IO;

namespace Skoda.DIAS.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddControllers();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddControllersWithViews();
            var TurnOffHttps = Configuration.GetValue<bool>("TurnOffHttps");

#if !DEBUG
            services.AddSession(options =>
            {
                // You might want to only set the application cookies over a secure connection:
                options.Cookie.SecurePolicy = TurnOffHttps ? CookieSecurePolicy.None : CookieSecurePolicy.Always;
                options.Cookie.SameSite = SameSiteMode.Strict;
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });
#else
            services.AddSession();
#endif
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = new PathString("/Admin/Login");
                options.Cookie.HttpOnly = true;
                options.Cookie.SecurePolicy = TurnOffHttps ? CookieSecurePolicy.None : CookieSecurePolicy.Always;
                options.Cookie.SameSite = SameSiteMode.Lax;
                options.Cookie.MaxAge = TimeSpan.FromDays(5);
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.Strict;
                options.HttpOnly = HttpOnlyPolicy.None;
                options.Secure = TurnOffHttps ? CookieSecurePolicy.None : CookieSecurePolicy.Always;
            });

            var connString = Configuration.GetConnectionString("ConnectionString");
            services.AddDbContext<SkodaContext>(optionsBuilder =>
            {
                optionsBuilder.UseLazyLoadingProxies().UseSqlServer(connString);
            });
            
            services.AddDataProtection().SetApplicationName("Skoda.DIAS")
                 .PersistKeysToFileSystem(new DirectoryInfo(Configuration.GetValue<string>("PersistKeysPath")));

            services.Configure<SMTPSettings>(Configuration.GetSection("SMTPSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Admin/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            var TurnOffHttps = Configuration.GetValue<bool>("TurnOffHttps");
            if (!TurnOffHttps)
            {
                app.UseHttpsRedirection();
            }

            app.UseSession();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Delear}/{action=Signup}");
            });
            
        }
    }
}
