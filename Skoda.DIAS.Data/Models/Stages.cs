﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Stages
    {
        public int Id { get; set; } 
        public string Description { get; set; } 
    }
}
