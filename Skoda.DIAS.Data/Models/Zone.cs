﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public  class Zone
    {
        public int Id { get; set; } 
        public string Name { get; set; } 
    }
}
