﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Delear_Enquiry
    {
        public int Dealer_ID { get; set; }
        public string Enquiry_id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Mobile_No { get; set; }
        public int City_Id { get; set; }
        public string Pincode { get; set; }
        public string Bussiness_Type { get; set; }
        public string Register_bussinessName { get; set; }
        public string Brand_name { get; set; }
        public string About_businessExp { get; set; }
        public string Source { get; set; }
        public string How_Know { get; set; }
        public string OtherLocation { get; set; } 
        public int Status_ID { get; set; } 
        public int BackgroundId { get; set; }
        public int State_Id { get; set; } 
        public DateTime Created_Date { get; set; }
        public bool isSalaried { get; set; }
       
    }
}
