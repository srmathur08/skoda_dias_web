﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class ApproverType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int DeptId { get; set; }
        public long RangeFrom { get; set; }
        public long RangeTo { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
