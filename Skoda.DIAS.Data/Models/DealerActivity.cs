﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{

    public partial class DealerActivity
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public DateTime Created_date { get; set; }
        public int Delear_ID { get; set; }   
    }
}
