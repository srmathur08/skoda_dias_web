﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Users
    {
        public Users()
        { 
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; } 
        public string Password { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public int CityId { get; set; }
        public string Mobile { get; set; }
        public DateTime CreatedOn { get; set; } 
        public bool IsDeleted { get; set; } 
        public virtual Roles Role { get; set; } 
        public virtual City City { get; set; }
    }
}
