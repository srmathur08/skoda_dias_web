﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Admin_FileUpload
    {
        public int Id { get; set; }
        public string FilePath { get; set; } 
        public int UserId { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedDate { get; set; } 
    }
}
