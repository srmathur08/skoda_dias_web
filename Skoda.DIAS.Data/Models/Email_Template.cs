﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{

    public partial class Email_Template
    {
        public int ID { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Template_ID { get; set; }
        public DateTime Created_date { get; set; } 
    }
}
