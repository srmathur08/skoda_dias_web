﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Skoda.DIAS.Data.Models
{
    public partial class SkodaContext : DbContext
    {
        public SkodaContext()
        {
        }

        public SkodaContext(DbContextOptions<SkodaContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Verification> Verification { get; set; }
        public virtual DbSet<Stages> Stages { get; set; }
        public virtual DbSet<AuditLog> AuditLog { get; set; }
        public virtual DbSet<UploadDocument> UploadDocument { get; set; }
        public virtual DbSet<DealerActivity> DealerActivity { get; set; }
        public virtual DbSet<Delear_Enquiry> Delear_Enquiry { get; set; }
        public virtual DbSet<Support_Email> Support_Email { get; set; }
        public virtual DbSet<Enquiry_Status> Enquiry_Status { get; set; }
        public virtual DbSet<Notifications> Notifications { get; set; } 
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<State> State { get; set; }
        public virtual DbSet<Zone> Zone { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Admin_FileUpload> Admin_FileUpload { get; set; }
        public virtual DbSet<Email_Template> Email_Template { get; set; }
        // public virtual DbSet<UserDelear_Enquiry> UserDelear_Enquiry { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Data Source=DESKTOP-2IAPAU3\\SQLEXPRESS;Initial Catalog=SkodaDIAS;Integrated Security=True;MultipleActiveResultSets=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
            modelBuilder.Entity<Notifications>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Message).IsUnicode(false);

                entity.Property(e => e.Title).IsUnicode(false);
                 
            });
              
            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Delear_Enquiry>(entity =>
            {
                entity.HasKey(e => e.Dealer_ID);
            });
            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasMaxLength(50); 

                entity.Property(e => e.Password).HasMaxLength(100);
                 
                 
                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50); 
                 

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_Roles");
                 
            });
              

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
