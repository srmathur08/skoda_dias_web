﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Notifications
    {
        public long Id { get; set; }
        public long? UserId { get; set; } 
        public string Title { get; set; }
        public string Message { get; set; }
        public long? DelearId { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; } 
    }
}
