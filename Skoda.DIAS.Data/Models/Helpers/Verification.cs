﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Verification
    {
        public int Id { get; set; }
        public int? Delear_ID { get; set; }
        public int SendCount { get; set; }
        public string Code { get; set; } 
    }
}
