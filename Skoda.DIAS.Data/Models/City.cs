﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class City
    { 
        public int Id { get; set; }
        public int ZoneId { get; set; }
        public int? StateId { get; set; }
        public string Pincode { get; set; }
        public bool IsOpen { get; set; }
        public string Name { get; set; } 
    }
    public partial class State
    {
        public int Id { get; set; }
        public int ZoneId { get; set; } 
        public string Name { get; set; }
    }
}
