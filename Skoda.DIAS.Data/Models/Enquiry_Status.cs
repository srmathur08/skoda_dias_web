﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Enquiry_Status
    {
        public int Id { get; set; }
        public string Enquiry_Id { get; set; }
        public int Delear_Id { get; set; }
        public int User_Id { get; set; }
        public int Stage_id { get; set; }
        public DateTime Created_Date { get; set; }
        public int Status_id { get; set; }
        public DateTime? ModifyDate { get; set; }
        // public bool Salaried { get; set; } 
    }
}
