﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class Support_Email
    {
        public int Id { get; set; }
        public int ZoneId { get; set; } 
        public string FullName { get; set; } 
        public string Email { get; set; } 
       
    }
}
