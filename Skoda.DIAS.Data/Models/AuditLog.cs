﻿using System;
using System.Collections.Generic;

namespace Skoda.DIAS.Data.Models
{
    public partial class AuditLog
    {
        public int Id { get; set; } 
        public int Delear_id { get; set; }
        public int Admin_id { get; set; }
        public int CurrentStatus_id { get; set; }
        public int UpdatedStatus_id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Description { get; set; }
    }
}
